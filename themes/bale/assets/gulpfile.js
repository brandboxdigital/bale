const gulp = require('gulp');
const webpack = require('webpack-stream');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const devBuild  = ((process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development');
const browsersync = devBuild ? require('browser-sync').create() : null;
const sourcemaps = devBuild ? require('gulp-sourcemaps') : null;
const noop = require('gulp-noop');
const newer = require('gulp-newer');
const size = require('gulp-size');
const imagemin = require('gulp-imagemin');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const atImport = require('postcss-import');
const terser = require('gulp-terser');


function images() {
  return gulp.src('src/images/*')
    .pipe(newer('images/'))
    .pipe(imagemin({ optimizationLevel: 5 }))
    .pipe(size({ showFiles: true }))
    .pipe(gulp.dest('images/'));
}

exports.images = images;


const cssConfig = {
  src: 'src/scss/main.scss',
  watch: 'src/scss/*.scss',
  build: 'css',
  sassOpts: {
    sourceMap: devBuild,
    imagePath: 'images/',
    precision: 3,
    errLogToConsole: true,
    includePaths:[
      './node_modules'
    ],
    },
  includePaths:[
    './node_modules'
  ],
  postCSS: [
    require('postcss-assets')({
      loadPaths: ['src/'],
      basePath: 'css'
    }),
    require('postcss-normalize'),
    require('autoprefixer')({
      browsers: ['> 1%']
    })
    // require('cssnano')
  ]
};

function css() {
  return gulp.src(cssConfig.src)
    .pipe(sourcemaps ? sourcemaps.init() : noop())
    .pipe(postcss(cssConfig.postCSS))
    .pipe(sass(cssConfig.sassOpts).on('error', sass.logError))
    .pipe(sourcemaps ? sourcemaps.write() : gulp.noop())
    .pipe(size({ showFiles: true }))
    .pipe(gulp.dest(cssConfig.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gulp.noop());
  // return (
  //     gulp
  //         .src(cssConfig.src)

  //         // Use sass with the files found, and log any errors
  //         .pipe(sass({
  //           includePaths: cssConfig.includePaths,
  //           outputStyle: 'compressed'
  //         }))
  //         .on("error", sass.logError)

  //         // What is the destination for the compiled file?
  //         .pipe(gulp.dest(cssConfig.build));
  // );
}

exports.css = css;


function html() {
  return gulp.src('src/*.html')
    .pipe(gulp.dest('build/'));
}

exports.html = html;


function font() {
  return gulp.src('src/fonts/*')
    .pipe(gulp.dest('fonts/'));
}

exports.font = font;


function js() {
  return gulp.src('src/js/main.js')
    .pipe(webpack({
      entry: './src/js/main.js',
      mode: 'development',
      output: { filename: 'main.js' },
      module: {
        rules: [
          { test: /\.js$/, exclude: '/node_modules', loader: 'babel-loader' }
        ]
      }
    }))
    .pipe(terser())
    .pipe(gulp.dest('./js/'));
}

exports.js = js;


const syncConfig = {
  server: {
    baseDir: './build',
    index: 'index.html'
  },
  port: 3000,
  open: false
};

function server(done) {
  if(browsersync)
    browsersync.init(syncConfig);

  done();
}

function watch(done) {
  gulp.watch('src/images/*', images);
  gulp.watch('src/scss/*.scss', css);
  // gulp.watch('src/*.html', html);
  gulp.watch('src/js/*.js', js);
  done();
}

function style_custom() {
    // Where should gulp look for the sass files?
    // My .sass files are stored in the styles folder
    // (If you want to use scss files, simply look for *.scss files instead)
    return (
        gulp
            .src(cssConfig.src)

            // Use sass with the files found, and log any errors
            .pipe(sass({
              includePaths: cssConfig.includePaths,
              outputStyle: 'compressed'
            }))
            .on("error", sass.logError)

            // What is the destination for the compiled file?
            .pipe(gulp.dest("css/"))
    );
}

exports.style_custom = style_custom;

function simple(){
    // gulp.simple takes in the location of the files to simple for changes
    // and the name of the function we want to run on change
    gulp.watch('src/scss/*.scss', style_custom)
}

// Don't forget to expose the task!
exports.simple = simple


exports.server = gulp.series(css, font, js, watch, server);
exports.build = gulp.series(css, font, js);

// exports.server = gulp.series(images, css, font, js, watch, server);
// exports.build = gulp.series(images, css, font, js);
