import $ from 'jquery';
window.jQuery = $;
require('lightslider');
require('webui-popover');

$(document).ready(function(){



  $('.header-mobile .burger').click(function() {
    $('.mobile-menu').fadeIn();
  });

  $('.mobile-menu .close').click(function() {
    $('.mobile-menu').fadeOut();
  });

  $('.house svg path').click(function() {
    if (!$(this).hasClass('active')) {
      let slide = $(this).data('slide');
      $('.gallery-container').removeClass('active');
      $('.house').addClass('active');
      $('.floors').addClass('active');
      $('.house svg path').removeClass('active');
      $(this).addClass('active');
      $('.floor-menu button').removeClass('active');
      $('.floor-menu button[data-floor="'+slide+'"]').addClass('active');
      $('.floors .main .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .main .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
      $('.floors .side .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .side .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
      if (document.documentElement.clientWidth < 810) {
        $('html, body').animate({
          scrollTop: $('#floors').offset().top
        }, 400)
      }
    }
  });

  $('.floor-menu button').click(function() {
    if (!$(this).hasClass('active')) {
      let slide = $(this).data('floor');
      $('.gallery-container').removeClass('active');
      $('.house svg path').removeClass('active');
      $('.house svg path[data-slide="'+slide+'"]').addClass('active');
      $('.floor-menu button').removeClass('active');
      $(this).addClass('active');
      $('.floor-menu button[data-floor="'+slide+'"]').addClass('active');
      $('.floors .main .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .main .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
      $('.floors .side .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .side .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
    }
  });

  $('.floors .floor svg path').click(function() {
    if (!$(this).hasClass('active')) {
      let slide = $(this).data('slide');
      $('.floors .main .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .main .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
      $('.floors .side .slide.active').removeClass('active').fadeOut(300, function() {
        $('.floors .side .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
      });
      $('.gallery-container').removeClass('active');
      $('.gallery-container[data-slide="'+slide+'"]').addClass('active');
      if (!$('.gallery-container[data-slide="'+slide+'"] .interior-slider').hasClass('lightSlider')) {
        $('.gallery-container[data-slide="'+slide+'"] .interior-slider').lightSlider({
          item: 2,
          loop: true,
          pager: false,
          slideMargin: 10
        });
      }
    }
  });

  $('.interier a').click(function (e) {
      e.preventDefault();

      document.querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth'
      });
  })

  $('.floors .back').click(function() {
    let slide = $(this).data('slide');
    console.log(slide);
    $('.gallery-container').removeClass('active');
    $('.floors .main .slide.active').removeClass('active').fadeOut(300, function() {
      $('.floors .main .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
    });
    $('.floors .side .slide.active').removeClass('active').fadeOut(300, function() {
      $('.floors .side .slide[data-slide="'+slide+'"]').addClass('active').fadeIn();
    });
  });



  $('.locations .all-locations button').click(function() {
    let id = $(this).data('location-group');
    $('.locations .inner.all').removeClass('active').fadeOut(300, function() {
      $('.locations .inner[data-slide="'+id+'"]').addClass('active').fadeIn(300);
      $('.map .dot-group[data-location-group="'+id+'"]').addClass('active').fadeIn();
    });
  });

  $('.locations .backbutton').click(function() {
    let id = $(this).data('slide');
    $(this).parents('.inner').removeClass('active').fadeOut(300, function() {
      $('.locations  .inner[data-slide="'+id+'"]').addClass('active').fadeIn(300);
      if (id == 'all') {
        $('.map .dot-group').removeClass('active').fadeOut();
        $('.map .dot-group[data-location-group="'+id+'"]').addClass('active').fadeIn();
      }
      $('.map .dot.active').removeClass('active');
    });
  });

  $('.locations .location-button').click(function() {
    let id = $(this).data('location');
    $(this).parents('.inner').removeClass('active').fadeOut(300, function() {
      $('.locations .inner[data-slide="'+id+'"]').addClass('active').fadeIn(300);
      $('.map .dot[data-location="'+id+'"]').addClass('active');
    });
  });

  $('.map .dot').click(function() {
    let id = $(this).data('location');
    if (!$('.locations .inner[data-slide="'+id+'"]').hasClass('active')) {
      $('.locations .inner.active').removeClass('active').fadeOut(300, function() {
        $('.locations .inner[data-slide="'+id+'"]').addClass('active').fadeIn(300);
        $('.map .dot.active').removeClass('active');
        $('.map .dot[data-location="'+id+'"]').addClass('active');
      });
    }
  });

  $( ".locations .location-button" ).hover(
    function() {
      $('.map .dot[data-location="'+$(this).data('location')+'"]').addClass('hover');
    }, function() {
      $('.map .dot[data-location="'+$(this).data('location')+'"]').removeClass('hover');
    }
  );

  $( ".map .dot" ).hover(
    function() {
      $('.locations .location-button[data-location="'+$(this).data('location')+'"]').addClass('hover');
    }, function() {
      $('.locations .location-button[data-location="'+$(this).data('location')+'"]').removeClass('hover');
    }
  );
});

 $(window).on('load', function () {
  var slider = $('.feature-slider').lightSlider({
    item: 3,
    loop: true,
    pager: false,
    slideMargin: 100
  });

  if ($('.feature-slider').length) {
    setTimeout(function() { slider.refresh(); }, 1000);
  }

  $('.alt-slider').lightSlider({
    item: 3,
    loop: true,
    pager: false,
    slideMargin: 10
  });

  $('.large-slider').lightSlider({
    item: 2,
    loop: true,
    pager: false,
    slideMargin: 10
  });

  var slider2 = $('.interrior-slider').lightSlider({
    item: 2,
    loop: true,
    pager: false,
    slideMargin: 10
  });

  $('section.expandable .section-heading').click(function(e) {
    e.preventDefault();
    var sectionHeading = $(e.target);

    if(!sectionHeading.hasClass('section-heading'))
      sectionHeading = sectionHeading.parents('.section-heading');

    var section = sectionHeading.parents('section.expandable');

    if(section.hasClass('expanded')) {
      section.removeClass('expanded');
      section.find('.content').slideUp();
    } else {
      section.addClass('expanded');
      section.find('.content').slideDown();
    }
  });

  $('.neighbors .item').webuiPopover({
    trigger: 'hover',
    placement: 'bottom',
    width: 360
  });
});
