-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 26, 2020 at 08:12 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brandbo4_bale`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '80.89.79.248', '2020-08-19 11:08:34', '2020-08-19 11:08:34'),
(2, 1, '80.89.79.248', '2020-08-19 11:09:27', '2020-08-19 11:09:27'),
(3, 1, '91.90.225.215', '2020-08-20 05:59:49', '2020-08-20 05:59:49'),
(4, 1, '212.3.198.188', '2020-08-20 10:24:50', '2020-08-20 10:24:50'),
(5, 1, '212.3.198.188', '2020-08-20 10:28:29', '2020-08-20 10:28:29'),
(6, 1, '212.3.198.188', '2020-08-20 10:35:59', '2020-08-20 10:35:59'),
(7, 1, '91.90.225.215', '2020-08-20 11:47:29', '2020-08-20 11:47:29'),
(8, 1, '91.90.225.215', '2020-08-20 13:11:46', '2020-08-20 13:11:46'),
(9, 1, '212.3.199.1', '2020-08-21 09:00:21', '2020-08-21 09:00:21'),
(10, 1, '91.90.225.215', '2020-08-21 11:44:39', '2020-08-21 11:44:39'),
(11, 1, '80.89.79.111', '2020-08-24 06:01:09', '2020-08-24 06:01:09'),
(12, 1, '80.89.79.111', '2020-08-24 06:03:50', '2020-08-24 06:03:50'),
(13, 1, '91.90.225.215', '2020-08-24 09:31:33', '2020-08-24 09:31:33'),
(14, 1, '91.90.225.215', '2020-08-25 11:13:21', '2020-08-25 11:13:21'),
(15, 2, '91.90.225.215', '2020-10-19 11:03:34', '2020-10-19 11:03:34'),
(16, 1, '91.90.225.215', '2020-10-19 12:06:02', '2020-10-19 12:06:02'),
(17, 2, '91.90.225.215', '2020-10-19 12:08:31', '2020-10-19 12:08:31'),
(18, 1, '91.90.225.215', '2020-10-20 07:38:54', '2020-10-20 07:38:54'),
(19, 2, '178.214.249.4', '2020-10-20 10:53:26', '2020-10-20 10:53:26'),
(20, 2, '188.170.79.85', '2020-10-24 07:50:53', '2020-10-24 07:50:53'),
(21, 2, '77.111.247.180', '2020-10-24 07:58:59', '2020-10-24 07:58:59'),
(22, 2, '91.90.225.215', '2020-10-26 06:08:40', '2020-10-26 06:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'bboxdigi', 'bale@brandbox.digital', '$2y$10$makM6l2H68uMuSrDSVeu.OTd1Pne1bSTCz9a/hu43r5se7qM3pWmW', NULL, NULL, NULL, '', 1, 2, NULL, '2020-10-20 07:38:54', '2020-08-19 11:07:30', '2020-10-26 06:07:52', NULL, 1),
(2, '', '', 'bale', 'admin@admin.com', '$2y$10$JP/dHvs8CI.sDN6W1ahjqu0dk9f0DAbNGSiVIZKo7UKN4YdINiU8W', NULL, '$2y$10$nJeJcsQQvORpz6oXe4SxL.ZZf.5nNzFLt9nBrNjHr1LgZrXDcuUgi', NULL, '', 0, 2, NULL, '2020-10-26 06:08:40', '2020-10-19 11:03:17', '2020-10-26 06:08:40', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2020-08-19 11:07:30', '2020-08-19 11:07:30', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2020-08-19 11:07:30', '2020-08-19 11:07:30'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2020-08-19 11:07:30', '2020-08-19 11:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '0.0.0.0', 0, NULL, 0, NULL, 0, NULL),
(2, 2, NULL, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_abouts`
--

CREATE TABLE `bboxdigi_bale_abouts` (
  `heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subheading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `history_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_owner_heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_owner_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighbors_heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighbors_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_abouts`
--

INSERT INTO `bboxdigi_bale_abouts` (`heading`, `subheading`, `properties`, `overview_link`, `overview_heading`, `overview_text`, `history_heading`, `history_text`, `first_owner_heading`, `first_owner_text`, `neighbors_heading`, `neighbors_text`, `id`, `created_at`, `updated_at`) VALUES
('ДОМ БАЛЛЕ -\r\nдух блистательного Петербурга', '«Дом Балле» расположен в исторической части города и рассчитан на 26 резиденций с видовыми характеристиками на историческую сцену Мариинского театра, Театральную площадь, сквер Консерватории и Никольский собор площадью от 210 до 440 кв.м.', 'Особенности дома', 'Обзор Резиденций', 'Исторический облик дома сочетается с последними технологиями строительства.', 'К выбору предлагаются 26 резиденций от 2-х до 3-х спален с лоджиями и террасами, площадью от 210 до 440 кв.м.  В каждой резиденции предусмотрены: прачечные, гардеробные, кабинет. В продаже также представлены 3 пентхауса с собственной террасой и двухуровневые апартаменты с индивидуальным выходом к Театральному скверу. Из резиденций открываются виды на Театральную площадь, двор-курдонер или купола Никольского собора.', 'История Дома Балле', 'В 1786 году земельный участок от Никольской улицы до Екатерининского канала был пожалован генерал-адьютанту Ивану Балле. Владелец разделил участок на четыре части, оставив за собой угловую. На плане Петербурга 1798 года на этом месте уже обозначен каменный дом, обращенный главным фасадом на площадь. В 1828 году архитектор Давид Висконти соединил здание с флигелем, а во дворе был разбит сад с фонтаном и нимфой. В 1895 году архитектор Федор Нагель вновь перестроил дом, сделав внешний фасад еще роскошнее и богаче. По настоящее время сложившийся облик фасада сохранен в духе эклектики прошлых лет, продолжая традиции петербургского модерна.', 'Первый владелец дома', 'Иван Петрович Балле (Баллей), будущий адмирал и сенатор, родился в 1741 году. В чине лейтенанта, командуя галерой, сопровождал Екатерину II в плавании по Волге в 1767 году. Проявил себя с наилучшей стороны как капитан Астраханского порта, потом как обер-экипажмейстер и в 1786 году стал обер-интендантом флота. Шведская война 1788-1790 годов выдвинула Балле как командира, участника Роченсальмского сражения. В 1801 году был переименован в адмиралы, также трудился в должности директора Училища корабельной архитектуры. Скончался в 1811 году.', 'Великие и знаменитые соседи', 'Среди «литературных» соседей Дома Балле были Лермонтов и Грибоедов (ул. Союза Печатников), Клюев и Есенин (набережная Фонтанки), Гоголь (ул. Декабристов) и Жуковский (набережная Крюкова канала) с 1904-1095 жил и работал художник Врубель (ул. Декабристов). Совсем рядом, на Театральной площади в 1909-1014 гг. жил режиссер Мейерхольд. Список мировых и отечественных знаменитостей, проживавших здесь в разные годы и эпохи, можно продолжать долго. Окрестности дома Балле слышали и помнят шаги Пушкина и Мандельштама, Кюхельбекера и Чернышевского, Баженова и Росси, Кшесинской и Комиссаржевской.', 1, '2020-08-19 11:07:29', '2020-08-20 07:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_contacts`
--

CREATE TABLE `bboxdigi_bale_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_form_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info@домбалле.ru',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '8 499 945-294-02',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Дом Балле, Санкт-Петербург,\nАдмиралтейский район, 190068\nул. Глинки, дом 4, литер «А»\n(Театральная площадь, 14.)',
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Запрос на обратный звонок',
  `tos` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Я согласен на обработку моих персональных данных',
  `submit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0тпрabить зaявку',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_contacts`
--

INSERT INTO `bboxdigi_bale_contacts` (`id`, `contact_form_email`, `email`, `phone`, `address`, `heading`, `tos`, `submit`, `created_at`, `updated_at`) VALUES
(1, 'test@example.com', 'Эксклюзивный брокер Engel & Völkers', '+7 812 244 09 99', 'Дом Балле, Санкт-Петербург,\r\nАдмиралтейский район, 190068\r\nул. Глинки, дом 4, литер «А»\r\n(Театральная площадь, 14.)', 'Запрос на обратный звонок', 'Я согласен на обработку моих персональных данных', '0тпрabить зaявку', '2020-08-19 11:07:29', '2020-08-22 09:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_flats`
--

CREATE TABLE `bboxdigi_bale_flats` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor_id` int(11) NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interrior` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exterrior` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ceiling` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bedrooms` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `plan_svg` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_flats`
--

INSERT INTO `bboxdigi_bale_flats` (`id`, `key`, `floor_id`, `number`, `interrior`, `exterrior`, `ceiling`, `bedrooms`, `wc`, `description`, `created_at`, `updated_at`, `plan_svg`) VALUES
(1, '1-1', 1, '1Л', '437,7 кв/м', '-', '3,82 м', '4', '6', 'Резиденция в двух уровнях. Окна верхнего уровня выходят на три стороны: на сквер, на ул. Глинки и Мариинский театр, и во двор-курдонер. Помимо прохода через общий холл, резиденция имеет собственный выход на Театральную площадь – в сквер, к памятнику Глинки. Проектом предусмотрены 4 спальни, гостиная, игровая и кабинет.', '2020-08-19 11:07:29', '2020-08-25 12:07:55', '<path data-slide=\"1-1\" d=\"M114 431H220.5V386H210V296H178.5V259.5H153.5V295H155.5V312.5H107V423.5L114 431Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(2, '1-2', 1, '2Л', '274,9  кв/м', '-', '3,05 м', '3', '4', 'Резиденция в двух уровнях. Окна выходят на сквер с памятником Глинке. Резиденция имеет отдельный выход на Театральную площадь, к скверу и проход через общий холл. Проектом предусмотрены 3 спальни, гостиная, кабинет.', '2020-08-19 11:07:29', '2020-08-26 06:07:40', '<path data-slide=\"1-2\" d=\"M155.5 312.5H107L106.5 110.5H213.5V157H176V174.5H153.5V295H155.5V312.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(3, '1-3', 1, '3Л', '258,2 кв/м', '-', '4.15 м', '3', '2+½', 'Резиденция на I этаже с окнами на три стороны: во двор-курдонер, на ул. Глинки и Мариинский театр, в малый двор. Проектом предусмотрены 3 спальни, игровая и гостиная.', '2020-08-19 11:07:29', '2020-08-26 06:06:37', '<path data-slide=\"1-3\" d=\"M489.5 431.5H381.5V260.5H452V349H497V425L489.5 431.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(5, '2-1', 2, '21', '323,0 кв/м', '6,5 кв/м балкон', '3,82 м', '3', '3+½', 'Резиденция с классической планировкой на II этаже. Окна на три стороны – в сквер, на Мариинский театр и во двор-курдонер. Классический балкон с выходом из гостиной, напротив памятника Глинке. Столовая с гостиной разделены, образуя анфиладу на пути в кабинет – с окнами на две стороны, прекрасным видом на театр и перспективу ул. Союза Печатников. Проектом предусмотрены 3 спальни, одна из которых большая – 47.5 м.', '2020-08-19 11:07:29', '2020-08-26 09:27:46', '<path data-slide=\"2-1\" d=\"M114 431.5H221V385.5H210.5V246H154V229.5H107V424L114 431.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(6, '2-2', 2, '22', '50,0 кв/м', '6,5 кв/м балкон', '3,82 м', '3', '3+½', 'Резиденция на II этаже с просторным залом: гостиная объединена с кухней-столовой. Классический балкон и окна выходят на сквер с памятником Глинке. С восточной стороны – лоджия. Проектом предусмотрены 3 спальни. Самая дальняя от ул. Глинки квартира на этаже.', '2020-08-19 11:07:29', '2020-08-26 09:30:05', '<path data-slide=\"2-2\" d=\"M107 229.5H154V188H176V174.5H193.5V157.5H232.5V96H245V71.5H107V141H91V197.5H107V229.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(7, '2-3', 2, '23', '230,5 кв/м', '11,9 кв/м лоджия', '3,82 м', '2', '2+½', 'Резиденция на II этаже с просторным залом: гостиная объединена с кухней-столовой, окна выходят во двор-курдонер. Проектом предусмотрены 2 спальни; обе имеют выход на просторную лоджию, обращенную на восток.', '2020-08-19 11:07:29', '2020-08-26 09:32:03', '<path data-slide=\"2-3\" d=\"M210.5 246V260L306.5 260V96H245V108.751H232.5V157.5H193.5V229H211V246H210.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(8, '2-4', 2, '24', '272,1 кв/м', '24,9 кв/м лоджия', '3,82 м', '3', '3+½', 'Резиденция на II этаже: гостиная объединена с кухней-столовой, окна выходят во двор-курдонер. 3 просторные лоджии, обращенные на восток. Проектом предусмотрены 3 спальни и кабинет; каждая спальня имеет выход на свою лоджию.', '2020-08-19 11:07:29', '2020-08-26 09:33:29', '<path data-slide=\"2-4\" d=\"M306.5 96V260H379.5V228.5H397V156.5H465V69.5H419V96H306.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(9, '3-1', 3, '31', '323,6 кв/м', '-', '3,22 м', '3', '3+½', 'Резиденция с классической планировкой на III этаже. Окна на три стороны – в сквер, на Мариинский театр и во двор-курдонер. Столовая с гостиной разделены, образуя анфиладу на пути в кабинет – с окнами на две стороны, прекрасным видом на театр и перспективу ул. Союза Печатников. Проектом предусмотрены 3 спальни, одна из которых большая – 47.5 м.', '2020-08-19 11:07:29', '2020-08-26 09:37:18', '<path data-slide=\"3-1\" d=\"M113.5 432.5H221V387.5H209.5V246.5H153V229.659H106V425L113.5 432.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(10, '3-2', 3, '32', '231,8 кв/м', '4,1 кв/м', '3,22 м', '3', '3+½', 'Резиденция на III этаже с просторным залом: гостиная объединена с кухней-столовой. Окна выходят на сквер с памятником Глинке. Лоджия на восточную сторону. Проектом предусмотрены 3 спальни. Самая дальняя от ул. Глинки квартира на этаже.', '2020-08-19 11:07:29', '2020-08-26 09:38:55', '<path data-slide=\"3-2\" d=\"M106 229.5H153V188H175V175H192.5V157H231.5V95H244.5V70.5H106V229.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(11, '3-3', 3, '33', '229,4 кв/м', '11,9 кв/м лоджия', '4.50 м', '2', '2+½', 'Резиденция на III этаже с просторным залом: гостиная объединена с кухней-столовой, их окна выходят во двор-курдонер. Проектом предусмотрены 2 спальни; обе имеют выход на просторную лоджию, обращенную на восток.', '2020-08-19 11:07:29', '2020-08-26 09:40:42', '<path data-slide=\"3-3\" d=\"M306 95H244.5V109.5H231.5V157H192.5V229H209.5V260H306V95Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(12, '3-4', 3, '34', '211,3 кв/м', '20,7 кв/м (2 лоджии)', '3,22 м', '2', '2+½', 'Резиденция на III этаже. Гостиная объединена с кухней-столовой, окна выходят во двор-курдонер. 2 просторные лоджии, обращенные на восток. Проектом предусмотрены 2 спальни и кабинет; каждая спальня имеет выход на свою лоджию.', '2020-08-19 11:07:29', '2020-08-26 09:42:32', '<path data-slide=\"3-4\" d=\"M418.708 95H306V260H379V229H396.5V156.5H427V109.5H418.708V95Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(13, '4-1', 4, '41', '323,0 кв/м', '-', '3,22 м', '3', '3+½', 'Резиденция с классической планировкой на IV этаже. Окна на три стороны – в сквер, на Мариинский театр и во двор-курдонер. Столовая с гостиной разделены, образуя анфиладу на пути в кабинет – с окнами на две стороны, прекрасным видом на театр и перспективу ул. Союза Печатников. Проектом предусмотрены 3 спальни, одна из которых большая – 49 м.', '2020-08-19 11:07:29', '2020-08-26 10:04:45', '<path data-slide=\"4-1\" d=\"M220.5 432V388H209.5V246.5H152.5V230H105.5V425L112.5 432H220.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(14, '4-2', 4, '42', '323,0 кв/м', '4,1 кв/м', '3,22 м', '3', '3+½', 'Резиденция на IV этаже с просторным залом: гостиная объединена с кухней-столовой. Окна выходят на сквер с памятником Глинке. Лоджия на восточную сторону. Проектом предусмотрены 3 спальни. Самая дальняя от ул. Глинки квартира на этаже.', '2020-08-19 11:07:29', '2020-08-26 10:18:19', '<path data-slide=\"4-2\" d=\"M152.5 230H105.5V71H244.5V95.5H232V157.5H192.5V175H175V188H152.5V230Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(15, '4-3', 4, '43', '229,7 кв/м', '11,9 кв/м лоджия', '3,22 м', '2', '2+½', 'Резиденция на IV этаже с просторным залом: гостиная объединена с кухней-столовой, их окна выходят во двор-курдонер. Проектом предусмотрены 2 спальни; обе имеют выход на просторную лоджию, обращенную на восток.', '2020-08-19 11:07:29', '2020-08-26 10:20:03', '<path data-slide=\"4-3\" d=\"M209.5 229.5V260H306V95.5H244.5V107.5H232V157.5H192.5V229.5H209.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(16, '4-4', 4, '44', '224,5 кв/м', '20,7 кв/м', '3,22 м', '2', '2+½', 'Резиденция на IV этаже. Гостиная объединена с кухней-столовой, окна выходят во двор-курдонер. 2 просторные лоджии, обращенные на восток. Проектом предусмотрены 2 спальни и кабинет; каждая спальня имеет выход на свою лоджию.', '2020-08-19 11:07:29', '2020-08-26 10:23:45', '<path data-slide=\"4-4\" d=\"M306 95.5V260H379V229H397V157H427.5V109.5H419V95.5H306Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(17, '2-6', 2, '26', '370,5 кв/м', '-', '3,82 м', '3', '3+½', 'Просторная резиденция на II этаже с окнами на три стороны: во двор-курдонер, на ул. Глинки, Мариинский театр и в малый двор. Проектом предусмотрены 3 спальни, гостиная, кухня-столовая и светлый кабинет с окнами на две стороны – в курдонер и на театр.', '2020-08-21 11:51:19', '2020-08-26 09:35:00', '<path data-slide=\"2-6\" d=\"M379.5 246.5H436.5V220.78H496.5V423.5L488.5 432H379.5V246.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(18, '3-5', 3, '35', '253,1 кв/м', '238,5 кв/м (терраса и лоджия)', '3,22 м', '3', '3+½', 'Резиденция на III этаже с огромной террасой (около 230 кв. м) и лоджией на восток. Окна выходят на террасу. Проектом предусмотрены 3 спальни.', '2020-08-21 11:55:06', '2020-08-26 09:43:44', '<path data-slide=\"3-5\" d=\"M396.5 174.5H414V188H436.5V260.5H514.5V70H419V109.5H427V156.5H396.5V174.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(19, '3-6', 3, '36', '331,7 кв/м', '-', '3,22 м', '3', '3+½', 'Просторная резиденция на III этаже с окнами на три стороны: во двор-курдонер, на ул. Глинки, Мариинский театр и в малый двор. Проектом предусмотрены 3 спальни. Гостиная, кухня-столовая и кабинет образуют анфиладу с окнами в парадный двор и на театр (кабинет).', '2020-08-21 11:56:07', '2020-08-26 09:44:37', '<path data-slide=\"3-6\" d=\"M380 246.5V433H489L496 426V260.5H436.5V246.5H380Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(20, '4-5', 4, '45', '253,0 кв/м', '4,2 кв/м', '3,22 м', '3', '3+½', 'Резиденция на IV этаже. Гостиная объединена с со столовой. Лоджия выходит на восток, окна – в малый двор. Проектом предусмотрены 3 спальни и кабинет.', '2020-08-21 11:57:53', '2020-08-26 10:24:54', '<path data-slide=\"4-5\" d=\"M414.5 157V188H437V260.5H514.843V70.5H419V95.5H430.5V109.5H427.5V157H414.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(21, '4-6', 4, '46', '331,5 кв/м', '3,22 кв/м', '3,22 м', '3', '3+½', 'Просторная резиденция на IV этаже с окнами на три стороны: во двор-курдонер, на ул. Глинки, Мариинский театр и в малый двор. Проектом предусмотрены 3 спальни. Гостиная, кухня-столовая и кабинет образуют анфиладу с окнами в парадный двор и на театр (кабинет).', '2020-08-21 12:01:50', '2020-08-26 10:32:56', '<path data-slide=\"4-6\" d=\"M497 260.5H437V246.309H381V433H489L497 425V260.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(22, '5-1', 5, '51', '261,5 кв/м', '59,6 кв/м', '3,22 м', '3', '3+½', 'Резиденция с классической планировкой на IV этаже. Окна на три стороны – в сквер, на Мариинский театр и во двор-курдонер. Просторная терраса, опоясывающая резиденцию по фасаду – с северо-запада на юго-запад, с видом на Консерваторию, Мариинский театр, перспективу улицы Союза Печатников и Никольский собор. Столовая с гостиной разделены, образуя анфиладу на пути в кабинет – с окнами на две стороны. Проектом предусмотрены 3 спальни.', '2020-08-21 12:03:00', '2020-08-26 10:44:04', '<path data-slide=\"5-1\" d=\"M118 419H208.5V261.5H211V246.5H154.5V230H118V419Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(23, '5-2', 5, '52', '205,6 кв/м', '29,7 кв/м', '3,22 м', '3', '3+½', 'Резиденция на V этаже с просторным залом: гостиная объединена с кухней-столовой. Просторная терраса вдоль фасада с видом на Консерваторию, Театральную площадь и сквер с памятником Глинке. Проектом предусмотрены 3 спальни. Самая дальняя от ул. Глинки квартира на этаже.', '2020-08-21 12:03:41', '2020-08-26 10:45:06', '<path data-slide=\"5-2\" d=\"M118 230H154.5V188.5H176V175H193.5V158H232.5V96H245.5V72.5H232.5H118V230Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(24, '5-3', 5, '53', '231,9 кв/м', '11,9 кв/м', '4.50 м', '2', '1+½', 'Резиденция на V этаже с просторным залом: гостиная объединена с кухней-столовой, их окна выходят во двор-курдонер. Проектом предусмотрены 2 спальни; обе имеют выход на просторную лоджию, обращенную на восток.', '2020-08-21 12:05:55', '2020-08-26 10:47:44', '<path data-slide=\"5-3\" d=\"M232.5 111H245.5V96.5H306.5V261.5H211V229H193.5V158H232.5V111Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(25, '5-4', 5, '54', '228,1 кв/м', '20,7 кв/м', '3,22 м', '2', '2+½', 'Резиденция на V этаже. Гостиная объединена с кухней-столовой, окна выходят во двор-курдонер. 2 просторные лоджии, обращенные на восток. Проектом предусмотрены 2 спальни и кабинет; каждая спальня имеет выход на свою лоджию.', '2020-08-21 12:06:30', '2020-08-26 10:48:28', '<path data-slide=\"5-4\" d=\"M306.5 261.5V96.5H419.5V111H426.5V157.5H396.5V229H379V261.5H306.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(26, '5-5', 5, '55', '243,9 кв/м', '11,9 кв/м', '3,22 м', '3', '3+½', 'Резиденция на V этаже. Гостиная объединена с со столовой.\r\nЛоджия выходит на восток, окна – в малый двор.\r\nПроектом предусмотрены 3 спальни и кабинет.', '2020-08-21 12:07:40', '2020-10-20 10:59:10', '<path data-slide=\"5-5\" d=\"M426.5 157.5V111H430V96.5H419V72H511.5V258H481V260.5H436V188.5H414V175H396.5V157.5H426.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>'),
(27, '5-6', 5, '56', '241,4 кв/м', '76,1 кв/м', '3,22 м', '3', '3+½', 'Просторная резиденция на V этаже с окнами на три стороны:\r\nво двор-курдонер, на ул. Глинки, Мариинский театр и в малый двор.\r\nПросторная терраса, опоясывающая резиденцию по фасаду –\r\nс северо-запада на юго-восток, с прекрасным видом\r\nна Мариинский театр. Проектом предусмотрены 3 спальни.\r\nГостиная, кухня-столовая и кабинет образуют анфиладу с окнами\r\nв парадный двор и на театр (кабинет).', '2020-08-21 12:08:26', '2020-08-26 10:49:44', '<path data-slide=\"5-6\" d=\"M436 260.5H481V419H390.5V261.5H379V246.5H436V260.5Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/>');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_floors`
--

CREATE TABLE `bboxdigi_bale_floors` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_floors`
--

INSERT INTO `bboxdigi_bale_floors` (`id`, `number`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(2, 2, 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(3, 3, 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(4, 4, 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(5, 5, 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor', '2020-08-21 12:02:08', '2020-08-21 12:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_footers`
--

CREATE TABLE `bboxdigi_bale_footers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info@домбалле.ru',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '8 499 945-294-02',
  `copyright` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Copyright 2019 Дом Балле',
  `design` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Design by brandbox.lv',
  `address` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Дом Балле Санкт-Петербург, Адмиралтейский район, 190068 ул. Глинки, дом 4, литер «А» (Театральная площадь, 14.)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_footers`
--

INSERT INTO `bboxdigi_bale_footers` (`id`, `email`, `phone`, `copyright`, `design`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Эксклюзивный брокер\r\nEngel & Völkers', '+7 812 244 09 99', 'Copyright 2019 Дом Балле', 'Design by brandbox.lv', 'Дом Балле Санкт-Петербург, Адмиралтейский район, 190068 ул. Глинки, дом 4, литер «А» (Театральная площадь, 14.)', '2020-08-19 11:07:29', '2020-08-20 08:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_headers`
--

CREATE TABLE `bboxdigi_bale_headers` (
  `id` int(10) UNSIGNED NOT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'О ДОМЕ',
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'МЕСТОПОЛОЖЕНИЕ',
  `infrastructure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ИНФРАСТРУКТУРА',
  `interrior` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ИНТЕРЬЕРЫ',
  `overview` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ОБЗОР РЕЗИДЕНЦИЙ',
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'КОНТАКТЫ',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '+7 812 770 22 57',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_headers`
--

INSERT INTO `bboxdigi_bale_headers` (`id`, `about`, `location`, `infrastructure`, `interrior`, `overview`, `contact`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'О ДОМЕ', 'МЕСТОПОЛОЖЕНИЕ', 'ИНФРАСТРУКТУРА', 'ИНТЕРЬЕРЫ', 'ОБЗОР РЕЗИДЕНЦИЙ', 'КОНТАКТЫ', '+7 812 244 09 99', '2020-08-19 11:07:28', '2020-08-20 06:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_index_pages`
--

CREATE TABLE `bboxdigi_bale_index_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_index_pages`
--

INSERT INTO `bboxdigi_bale_index_pages` (`id`, `heading`, `created_at`, `updated_at`) VALUES
(1, 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней', '2020-08-19 11:07:28', '2020-08-19 11:07:28');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_infrastructures`
--

CREATE TABLE `bboxdigi_bale_infrastructures` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней',
  `subheading` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.',
  `subheading2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Общие технические системы и решения',
  `overview_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Обзор Резиденций',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_infrastructures`
--

INSERT INTO `bboxdigi_bale_infrastructures` (`id`, `heading`, `subheading`, `subheading2`, `overview_link`, `created_at`, `updated_at`) VALUES
(1, 'ДОМ БАЛЛЕ -\r\nимперская роскошь\r\nнаших дней', 'Инфраструктура дома Балле с расширенным сервисом класса De Luxe исключительно для жильцов дома. Включает в себя закрытую и охраняемую территорию с круглосуточным видеонаблюдением, консьерж службу, батлер-сервис и Wellness центр с бассейном.', 'Общие технические системы и решения', 'Обзор Резиденций', '2020-08-19 11:07:29', '2020-08-20 07:35:22'),
(2, 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней', 'Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.', 'Общие технические системы и решения', 'Обзор Резиденций', '2020-08-20 10:39:39', '2020-08-20 10:39:39'),
(3, 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней', 'Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.', 'Общие технические системы и решения', 'Обзор Резиденций', '2020-08-24 06:03:50', '2020-08-24 06:03:50');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_infrastructure_points`
--

CREATE TABLE `bboxdigi_bale_infrastructure_points` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_infrastructure_points`
--

INSERT INTO `bboxdigi_bale_infrastructure_points` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Консьерж - сервис', 'Kруглосуточное обслуживание взыскательных жильцов с повышенным уровнем комфорта и соблюдением конфиденциальности. К распоряжению резидентов услуги прачечной, химчистки, заказ такси, авиабилетов, билетов на мероприятия и т.д.', '2020-08-19 11:07:29', '2020-08-20 07:47:46'),
(2, 'Wellness центр', 'Wellness – центр представлен плавательным бассейном, сауной, хаммамом, массажным кабинетом, фитнес-баром, залом для парикмахера и стилиста и работающий по системе закрытого клуба – только для собственников резиденций. Все элементы выполнены из высококачес', '2020-08-19 11:07:29', '2020-08-20 07:48:22'),
(3, 'Зал для парикмахера', 'Дополнительная зона для ухода за собой предусмотрена в зале для Парикмахера, куда можно пригласить личного стилиста и визажиста.', '2020-08-19 11:07:29', '2020-08-20 07:40:46'),
(4, 'Тренажерный зал', 'Зал для занятия спортом и йогой оснащен тренажерами нового поколения премиум-класса. Зал работает по системе закрытого клуба – только для собственников резиденций, с возможностью проведения индивидуальных тренировок.', '2020-08-19 11:07:29', '2020-08-20 07:42:32'),
(5, 'Клубная комната', 'Для проведения важных деловых встреч, а также собеседования персонала в «Доме Балле» предусмотрена Клубная комната для переговоров, с доступом к высокоскоростному интернету Wi Fi.', '2020-08-19 11:07:29', '2020-08-20 07:43:17'),
(6, 'Помещение для хранения вин', 'Инфраструктура дома включает в себя помещение для хранения вин, в котором поддерживается необходимая температура и микроклимат. Рядом – элегантная и комфортабельная сигарная комната со специальными ячейками для персональных хьюмидоров и шкафами для ликеро', '2020-08-19 11:07:29', '2020-08-20 07:44:17'),
(7, 'Kамера для хранения изделий из меха', 'Для комфортного проживания резидентов предусмотрены камеры для хранения вещей «от кутюр», а также ценных меховых изделий. Камеры обеспечат идеальную температуру и сохранность вещей.', '2020-08-19 11:07:29', '2020-08-20 07:45:14'),
(8, 'Кладовые-ячейки', 'В отдельном отсеке дома находятся ячейки-кладовые, в которых резидентам будет удобно хранить спортивный инвентарь и сезонные вещи.', '2020-08-19 11:07:29', '2020-08-20 07:46:11'),
(10, 'Подземный паркинг', 'Просторный подземный паркинг с круглосуточным видеонаблюдением. Парковочные места представлены двумя типами: стандартное машиноместо и семейное. У каждого резидента есть возможность пробрести порядка 2-х машиномест.', '2020-08-20 13:00:33', '2020-08-20 13:00:33');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_infrastructure_solutions`
--

CREATE TABLE `bboxdigi_bale_infrastructure_solutions` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_infrastructure_solutions`
--

INSERT INTO `bboxdigi_bale_infrastructure_solutions` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'водопровод: два независимых ввода и система очистки', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(2, 'два независимых источника электроснабжения, встроенный ТП 10/04', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(3, 'единая сеть WiFi во всем здании высокоскоростной интернет, ТВ', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(4, 'IP-телефония от одного из двух ведущих провайдеров на выбор собственника', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(5, 'каждые апартаменты оборудованы собственной вентиляционной системой машиной', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(6, 'единая система контроля кондиционирования, отопления, влажности дает возможность установки индивидуальных параметров на каждое помещение', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(7, 'система усиления зоны мобильной связи во всех помещениях, включая лифтовые кабины и паркинг', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(8, 'охранная система каждой квартиры интегрирована в единый комплекс автоматизации апартаментов', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(9, 'персонализированный контроль доступа в помещения здания', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(10, 'видеонаблюдение в общих зонах', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(11, 'теплоснабжение обеспечивают два источника – городская теплосеть и автономный источник на природном газе', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(12, 'разграничение прав доступа к индивидуальным лифтам', '2020-08-19 11:07:29', '2020-08-19 11:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_interriors`
--

CREATE TABLE `bboxdigi_bale_interriors` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней',
  `subheading` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.',
  `caption1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'KRASSKY',
  `text1` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Основанная в Латвии международная компания – KRASSKY – принимала участие в работе над отделкой̆ Дома Балле.\nКлиентами KRASSKY являются люди с высокими требованиями к сервису и богатым жизненным опытом. Мы считаем, что хороший̆ интерьер подчеркивает личность и стиль жизни его пользователя, поэтому для каждого клиента создаются индивидуальные решения. Мы верим, что хороший̆ дизайн улучшает качество жизни, долговечен и функционален.',
  `address1` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '119021, Москва, ул. Тимура Фрунзе 11/1\n+7 (495) 181-53-00\nkrassky@krassky.ru',
  `caption2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'KRASSKY',
  `text2` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Основанная в Латвии международная компания – KRASSKY – принимала участие в работе над отделкой̆ Дома Балле.\nКлиентами KRASSKY являются люди с высокими требованиями к сервису и богатым жизненным опытом. Мы считаем, что хороший̆ интерьер подчеркивает личность и стиль жизни его пользователя, поэтому для каждого клиента создаются индивидуальные решения. Мы верим, что хороший̆ дизайн улучшает качество жизни, долговечен и функционален.',
  `address2` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '119021, Москва, ул. Тимура Фрунзе 11/1\n+7 (495) 181-53-00\nkrassky@krassky.ru',
  `overview_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Обзор Резиденций',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_interriors`
--

INSERT INTO `bboxdigi_bale_interriors` (`id`, `heading`, `subheading`, `caption1`, `text1`, `address1`, `caption2`, `text2`, `address2`, `overview_link`, `created_at`, `updated_at`) VALUES
(1, 'Изысканные интерьеры \r\nДОМА БАЛЛЕ', 'Дом Балле – исторический особняк начала XIX века с панорамными видами на площадь Мариинского театра, Никольский собор и консерваторию им. Римского-Корсакова. Закрытый внутренний двор-курдонер с фонтаном «Девушка с кувшином» привлекает вниманием прохожих. Внутренние интерьеры дома не уступают в своем великолепии.', 'Парадное лобби', 'Внутреннее убранство особняка оформлено в стиле раннего Ар-деко́. Парадное лобби выступило центральной доминантой «Дома Балле», атмосферу роскоши и благородства создают стойка ресепшн, пол и колонны, выполненные из натурального мрамора, деревянные панели и мебель от ведущих европейских брендов. Венчают ослепительный ансамбль оригинальные предметы искусства.', '', 'Натуральные материалы', 'В проекте гармонично сочетается историческое прошлое и современные технологии строительства. Интерьеры дома оформлены с использованием, исключительно, натуральных материалов: редких пород древесины – сахарного клена, карельской березы  его интерьер оформлен с использованием, исключительно, натуральных материалов: редких пород древесины – сахарного клена, карельской березы.', '', 'Обзор Резиденций', '2020-08-19 11:07:29', '2020-08-20 08:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_locations`
--

CREATE TABLE `bboxdigi_bale_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_category_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_x` double(8,2) NOT NULL,
  `position_y` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_locations`
--

INSERT INTO `bboxdigi_bale_locations` (`id`, `location_category_id`, `name`, `description`, `distance`, `position_x`, `position_y`, `created_at`, `updated_at`) VALUES
(1, 1, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.33, 0.40, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(2, 1, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.23, 0.50, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(3, 1, 'Римско-католический храм Св. Станислава', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '650 м', 0.23, 0.50, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(4, 2, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.43, 0.30, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(5, 2, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.45, 0.26, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(6, 2, 'Римско-католический храм Св. Станислава', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '650 м', 0.53, 0.80, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(7, 3, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.53, 0.30, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(8, 3, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.43, 0.60, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(9, 3, 'Римско-католический храм Св. Станислава', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '650 м', 0.25, 0.76, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(10, 4, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.45, 0.26, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(11, 4, 'Никольский сад', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '220 м', 0.23, 0.90, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(12, 4, 'Римско-католический храм Св. Станислава', 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.', '650 м', 0.53, 0.50, '2020-08-19 11:07:29', '2020-08-19 11:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_location_categories`
--

CREATE TABLE `bboxdigi_bale_location_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_location_categories`
--

INSERT INTO `bboxdigi_bale_location_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Сакральные объекты', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(2, 'Музеи', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(3, 'Театры', '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(4, 'Рестораны', '2020-08-19 11:07:29', '2020-08-19 11:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `bboxdigi_bale_neighbors`
--

CREATE TABLE `bboxdigi_bale_neighbors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bboxdigi_bale_neighbors`
--

INSERT INTO `bboxdigi_bale_neighbors` (`id`, `name`, `created_at`, `updated_at`, `description`) VALUES
(1, 'Михаил Лермонтов', '2020-08-19 11:07:29', '2020-08-20 11:54:46', '(1814-1841) — русский поэт, прозаик, драматург, художник.'),
(2, 'Николай Гоголь', '2020-08-19 11:07:29', '2020-08-20 11:51:14', '(1809-1852) — русский прозаик, драматург, поэт, критик, публицист, признанный одним из классиков русской литературы.'),
(3, 'Модест Мусоргский', '2020-08-19 11:07:29', '2020-08-20 11:52:35', '(1839-1881) — русский композитор, член «Могучей кучки».'),
(4, 'Александр Блок', '2020-08-19 11:07:29', '2020-08-20 11:48:32', '(1880-1921) — русский поэт, писатель, публицист, драматург, переводчик, литературный критик. Классик русской литературы XX столетия, один из крупнейших представителей русского символизма.'),
(5, 'Петр Чайковский', '2020-08-19 11:07:29', '2020-08-20 11:55:19', '(1840-1893) — русский композитор, педагог, дирижёр и музыкальный критик.'),
(6, 'Михаил Глинка', '2020-08-19 11:07:29', '2020-08-20 11:51:33', '(1804-1857) — русский композитор.'),
(7, 'Александр Серов', '2020-08-19 11:07:29', '2020-08-20 11:52:18', '(1820-1871) — русский композитор и музыкальный критик; действительный статский советник; отец живописца Валентина Серова.'),
(8, 'Александр Пушкин', '2020-08-19 11:07:29', '2020-08-20 11:52:55', '(1799-1837) — русский поэт, драматург и прозаик, заложивший основы русского реалистического направления, критик и теоретик литературы, историк, публицист.');

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deferred_bindings`
--

INSERT INTO `deferred_bindings` (`id`, `master_type`, `master_field`, `slave_type`, `slave_id`, `session_key`, `is_bind`, `created_at`, `updated_at`) VALUES
(77, 'Bboxdigi\\Bale\\Models\\Footer', 'picture', 'System\\Models\\File', '2', 'txUWMCJ68r33gQQ8k6LYSu74g26q0xV8zBcZUaPE', 0, '2020-08-21 08:47:52', '2020-08-21 08:47:52'),
(138, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '62', 'hvPHr7djJUZVmieNUvq0fzVaBFG8Kk4pYtoOdQfR', 0, '2020-08-25 07:26:47', '2020-08-25 07:26:47'),
(139, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '150', 'hvPHr7djJUZVmieNUvq0fzVaBFG8Kk4pYtoOdQfR', 1, '2020-08-25 07:26:55', '2020-08-25 07:26:55'),
(140, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '62', 'wkFj4FszY8Yt1Ntm2aqIKg2ndokOIgjsUFh6Guf5', 0, '2020-08-25 07:30:37', '2020-08-25 07:30:37'),
(141, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '151', 'wkFj4FszY8Yt1Ntm2aqIKg2ndokOIgjsUFh6Guf5', 1, '2020-08-25 07:30:43', '2020-08-25 07:30:43'),
(142, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '62', 'lnPFnWZcuCpgAo5hs07IuVQlxkBzFBgo9iF7mLgz', 0, '2020-08-25 07:40:16', '2020-08-25 07:40:16'),
(143, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '152', 'lnPFnWZcuCpgAo5hs07IuVQlxkBzFBgo9iF7mLgz', 1, '2020-08-25 07:40:22', '2020-08-25 07:40:22'),
(145, 'Bboxdigi\\Bale\\Models\\About', 'pictures1', 'System\\Models\\File', '62', '08RTE5PHCu92S04rKFlOHGsZf25q5ybSmJMz6ik2', 0, '2020-08-25 07:51:27', '2020-08-25 07:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `indikator_backend_trash`
--

CREATE TABLE `indikator_backend_trash` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'en', 'English', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_user_mail_blockers`
--

CREATE TABLE `rainlab_user_mail_blockers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Twig\\Error\\SyntaxError: An array element must be followed by a comma. Unexpected token \"arrow function\" of value \"=>\" (\"punctuation\" expected with value \",\") in \"/home/brandbo4/web/digital.brandbox.backend.bale/themes/bale/pages/about.htm\" at line 123. in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/TokenStream.php:76\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(329): Twig\\TokenStream->expect(9, \',\', \'An array elemen...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(279): Twig\\ExpressionParser->parseArrayExpression()\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(593): Twig\\ExpressionParser->parseExpression(0, false)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(477): Twig\\ExpressionParser->parseArguments()\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(395): Twig\\ExpressionParser->parseSubscriptExpression(Object(Twig\\Node\\Expression\\GetAttrExpression))\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(289): Twig\\ExpressionParser->parsePostfixExpression(Object(Twig\\Node\\Expression\\GetAttrExpression))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Parser.php(142): Twig\\ExpressionParser->parseExpression()\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/TokenParser/ForTokenParser.php(52): Twig\\Parser->subparse(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Parser.php(185): Twig\\TokenParser\\ForTokenParser->parse(Object(Twig\\Token))\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass(\'__TwigTemplate_...\', \'/home/brandbo4/...\', NULL)\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/cms/classes/Controller.php(422): Twig\\Environment->loadTemplate(\'/home/brandbo4/...\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/cms/classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'about\')\n#21 [internal function]: Cms\\Classes\\CmsController->run(\'about\')\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#61 {main}', NULL, '2020-08-20 07:41:32', '2020-08-20 07:41:32');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(2, 'error', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:77\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php(77): PDO->prepare(\'update `bboxdig...\', Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'ZZEXz8nqGD6Stec...\')\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#18 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#21 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#26 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:79\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'ZZEXz8nqGD6Stec...\')\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#17 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#20 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#25 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' (SQL: update `bboxdigi_bale_neighbors` set `updated_at` = 2020-08-20 12:28:17, `description` = description 123 where `id` = 5) in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'ZZEXz8nqGD6Stec...\')\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#15 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#18 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#23 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2020-08-20 10:28:17', '2020-08-20 10:28:17');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(3, 'error', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:77\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php(77): PDO->prepare(\'update `bboxdig...\', Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'rUzxep7LBFhCdza...\')\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#18 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#21 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#26 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:79\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'rUzxep7LBFhCdza...\')\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#17 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#20 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#25 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' (SQL: update `bboxdigi_bale_neighbors` set `updated_at` = 2020-08-20 12:28:40, `description` = 123 where `id` = 5) in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'rUzxep7LBFhCdza...\')\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#15 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#18 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#23 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2020-08-20 10:28:40', '2020-08-20 10:28:40'),
(4, 'error', 'Symfony\\Component\\Console\\Exception\\CommandNotFoundException: Command \"migrate\" is not defined. in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php:646\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(236): Symfony\\Component\\Console\\Application->find(\'migrate\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Console/Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#5 {main}', NULL, '2020-08-20 10:35:02', '2020-08-20 10:35:02'),
(5, 'error', 'Symfony\\Component\\Console\\Exception\\CommandNotFoundException: Command \"migrate\" is not defined. in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php:646\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(236): Symfony\\Component\\Console\\Application->find(\'migrate\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Console/Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#5 {main}', NULL, '2020-08-20 10:35:06', '2020-08-20 10:35:06');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(6, 'error', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:77\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php(77): PDO->prepare(\'update `bboxdig...\', Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'XLNgdmikNPrRCoy...\')\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#18 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#21 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#26 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:79\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'XLNgdmikNPrRCoy...\')\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#17 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#20 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#25 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'description\' in \'field list\' (SQL: update `bboxdigi_bale_neighbors` set `updated_at` = 2020-08-20 12:36:11, `description` = 1 where `id` = 5) in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(320): October\\Rain\\Database\\Model->save(NULL, \'XLNgdmikNPrRCoy...\')\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/ManagesTransactions.php(29): Backend\\Behaviors\\FormController->Backend\\Behaviors\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/FormController.php(322): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#15 [internal function]: Backend\\Behaviors\\FormController->update_onSave(\'5\')\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'update_onSave\', Array)\n#18 [internal function]: Backend\\Classes\\Controller->__call(\'update_onSave\', Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(620): call_user_func_array(Array, Array)\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onSave\')\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#23 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/n...\')\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2020-08-20 10:36:11', '2020-08-20 10:36:11');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(7, 'error', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:77\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php(77): PDO->prepare(\'update `bboxdig...\', Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'zQEdXXa9VPXhAO2...\')\n#14 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#17 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#22 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#62 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:79\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'zQEdXXa9VPXhAO2...\')\n#13 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#16 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#21 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#61 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' (SQL: update `bboxdigi_bale_flats` set `updated_at` = 2020-08-24 08:02:00, `plan_svg` = <path data-slide=\"1-1\" d=\"M224 383H214V230.5H193.5V247H153V230.5H103V424L114 435H224V383Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/> where `id` = 1) in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'zQEdXXa9VPXhAO2...\')\n#11 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#14 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#19 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#59 {main}', NULL, '2020-08-24 06:02:00', '2020-08-24 06:02:00');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(8, 'error', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:77\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php(77): PDO->prepare(\'update `bboxdig...\', Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'39zjsfJksB21JHe...\')\n#14 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#17 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#22 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#61 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#62 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/PDOConnection.php:79\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(479): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'update `bboxdig...\')\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'update `bboxdig...\', Array)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'39zjsfJksB21JHe...\')\n#13 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#16 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#21 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#59 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#60 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#61 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'plan_svg\' in \'field list\' (SQL: update `bboxdigi_bale_flats` set `updated_at` = 2020-08-24 08:02:54, `plan_svg` = <path data-slide=\"1-1\" d=\"M224 383H214V230.5H193.5V247H153V230.5H103V424L114 435H224V383Z\" fill=\"#EDE3CE\" stroke=\"#EDE3CE\"/> where `id` = 1) in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php:664\nStack trace:\n#0 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'update `bboxdig...\', Array, Object(Closure))\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(490): Illuminate\\Database\\Connection->run(\'update `bboxdig...\', Array, Object(Closure))\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Connection.php(423): Illuminate\\Database\\Connection->affectingStatement(\'update `bboxdig...\', Array)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php(2173): Illuminate\\Database\\Connection->update(\'update `bboxdig...\', Array)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(251): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(780): October\\Rain\\Database\\QueryBuilder->update(Array)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(628): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(543): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(October\\Rain\\Database\\Builder))\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(761): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/Model.php(794): October\\Rain\\Database\\Model->saveInternal(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/behaviors/RelationController.php(1153): October\\Rain\\Database\\Model->save(NULL, \'39zjsfJksB21JHe...\')\n#11 [internal function]: Backend\\Behaviors\\RelationController->onRelationManageUpdate(\'1\')\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Extension/ExtendableTrait.php(414): call_user_func_array(Array, Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(184): Backend\\Classes\\Controller->extendableCall(\'onRelationManag...\', Array)\n#14 [internal function]: Backend\\Classes\\Controller->__call(\'onRelationManag...\', Array)\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(628): call_user_func_array(Array, Array)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(478): Backend\\Classes\\Controller->runAjaxHandler(\'onRelationManag...\')\n#17 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/Controller.php(277): Backend\\Classes\\Controller->execAjaxHandlers()\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(165): Backend\\Classes\\Controller->run(\'update\', Array)\n#19 [internal function]: Backend\\Classes\\BackendController->run(\'bboxdigi/bale/f...\')\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/modules/backend/classes/BackendController.php(68): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(131): Backend\\Classes\\BackendController->Backend\\Classes\\{closure}(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#47 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#48 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#49 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#50 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(25): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#53 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#57 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#58 /home/brandbo4/web/digital.brandbox.backend.bale/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#59 {main}', NULL, '2020-08-24 06:02:54', '2020-08-24 06:02:54'),
(9, 'error', 'ErrorException: file_put_contents(/home/brandbo4/web/digital.brandbox.backend.bale/storage/framework/cache/97/39/9739b3a900e31d6b7467fc7f7b244804b462b524): failed to open stream: Disk quota exceeded in /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:122\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'file_put_conten...\', \'/home/brandbo4/...\', 122, Array)\n#1 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(122): file_put_contents(\'/home/brandbo4/...\', \'1598447856a:1:{...\', 2)\n#2 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Filesystem/Filesystem.php(217): Illuminate\\Filesystem\\Filesystem->put(\'/home/brandbo4/...\', \'1598447856a:1:{...\', true)\n#3 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cache/FileStore.php(65): October\\Rain\\Filesystem\\Filesystem->put(\'/home/brandbo4/...\', \'1598447856a:1:{...\', true)\n#4 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cache/Repository.php(191): Illuminate\\Cache\\FileStore->put(\'system_paramete...\', Array, 5)\n#5 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cache/Repository.php(323): Illuminate\\Cache\\Repository->put(\'system_paramete...\', Array, 5)\n#6 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Cache/CacheManager.php(304): Illuminate\\Cache\\Repository->remember(\'system_paramete...\', 5, Object(Closure))\n#7 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(158): Illuminate\\Cache\\CacheManager->__call(\'remember\', Array)\n#8 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(121): October\\Rain\\Database\\QueryBuilder->getCached(Array)\n#9 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/october/rain/src/Database/QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#10 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(481): October\\Rain\\Database\\QueryBuilder->get(Array)\n#11 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(465): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#12 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Database/Concerns/BuildsQueries.php(77): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#13 /home/brandbo4/web/digital.brandbox.backend.bale/modules/system/models/Parameter.php(121): Illuminate\\Database\\Eloquent\\Builder->first()\n#14 /home/brandbo4/web/digital.brandbox.backend.bale/modules/system/models/Parameter.php(74): System\\Models\\Parameter::findRecord(\'system::update....\')\n#15 /home/brandbo4/web/digital.brandbox.backend.bale/modules/system/classes/UpdateManager.php(157): System\\Models\\Parameter::set(\'system::update....\', 0)\n#16 /home/brandbo4/web/digital.brandbox.backend.bale/modules/system/console/OctoberUp.php(35): System\\Classes\\UpdateManager->update()\n#17 [internal function]: System\\Console\\OctoberUp->handle()\n#18 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(29): call_user_func_array(Array, Array)\n#19 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#20 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#21 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Container/Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#22 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Console/Command.php(183): Illuminate\\Container\\Container->call(Array)\n#23 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Command/Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#24 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Console/Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#25 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(987): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#26 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(255): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#27 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/symfony/console/Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#28 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Console/Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 /home/brandbo4/web/digital.brandbox.backend.bale/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 /home/brandbo4/web/digital.brandbox.backend.bale/artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 {main}', NULL, '2020-08-26 11:12:36', '2020-08-26 11:12:36');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, '5f3d2410f28ce522242499.png', 'logo.png', 70935, 'image/png', NULL, NULL, 'logo', '1', 'Bboxdigi\\Bale\\Models\\Header', 1, 1, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(3, '5f3d24111bd9f521179797.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '1', 'Bboxdigi\\Bale\\Models\\Location', 1, 3, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(4, '5f3d24111e71c883927592.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '2', 'Bboxdigi\\Bale\\Models\\Location', 1, 4, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(5, '5f3d24111f671869181652.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '3', 'Bboxdigi\\Bale\\Models\\Location', 1, 5, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(6, '5f3d241120b31807721633.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '4', 'Bboxdigi\\Bale\\Models\\Location', 1, 6, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(7, '5f3d2411217ac767020623.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '5', 'Bboxdigi\\Bale\\Models\\Location', 1, 7, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(8, '5f3d2411223a9048416933.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '6', 'Bboxdigi\\Bale\\Models\\Location', 1, 8, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(9, '5f3d241123236853183103.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '7', 'Bboxdigi\\Bale\\Models\\Location', 1, 9, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(10, '5f3d241123e89773978503.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '8', 'Bboxdigi\\Bale\\Models\\Location', 1, 10, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(11, '5f3d2411249f4059408567.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '9', 'Bboxdigi\\Bale\\Models\\Location', 1, 11, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(12, '5f3d241125906052326113.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '10', 'Bboxdigi\\Bale\\Models\\Location', 1, 12, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(13, '5f3d241126693349956217.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '11', 'Bboxdigi\\Bale\\Models\\Location', 1, 13, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(14, '5f3d24112712c399539625.jpg', 'location_placeholder.jpg', 20686, 'image/jpeg', NULL, NULL, 'picture', '12', 'Bboxdigi\\Bale\\Models\\Location', 1, 14, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(23, '5f3d24114b664698085210.svg', 'inf01.svg', 497, 'image/svg', NULL, NULL, 'picture', '1', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 23, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(24, '5f3d24114dcb3539295778.svg', 'inf02.svg', 412, 'image/svg', NULL, NULL, 'picture', '2', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 24, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(25, '5f3d24114e79e265931690.svg', 'inf03.svg', 766, 'image/svg', NULL, NULL, 'picture', '3', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 25, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(26, '5f3d24114f018647784384.svg', 'inf04.svg', 707, 'image/svg', NULL, NULL, 'picture', '4', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 26, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(27, '5f3d24114f8da043090726.svg', 'inf05.svg', 752, 'image/svg', NULL, NULL, 'picture', '5', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 27, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(28, '5f3d24115066a643872129.svg', 'inf06.svg', 660, 'image/svg', NULL, NULL, 'picture', '6', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 28, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(29, '5f3d241151509725955192.svg', 'inf07.svg', 615, 'image/svg', NULL, NULL, 'picture', '7', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 29, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(30, '5f3d2411524ac846167069.svg', 'inf08.svg', 684, 'image/svg', NULL, NULL, 'picture', '8', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 30, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(31, '5f3d241153102473271246.svg', 'inf09.svg', 766, 'image/svg', NULL, NULL, 'picture', '9', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 31, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(32, '5f3d2411541a8152291585.svg', 'inf10.svg', 1123, 'image/svg', NULL, NULL, 'picture', '10', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 32, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(33, '5f3d241154f8f524929865.svg', 'inf11.svg', 2088, 'image/svg', NULL, NULL, 'picture', '11', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 33, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(34, '5f3d2411559ec198726299.svg', 'inf12.svg', 1243, 'image/svg', NULL, NULL, 'picture', '12', 'Bboxdigi\\Bale\\Models\\InfrastructureSolution', 1, 34, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(48, '5f3d24118c422847202898.jpg', 'parmaju3.jpg', 87745, 'image/jpeg', NULL, NULL, 'pictures3', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 48, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(49, '5f3d24118c668482883481.jpg', 'parmaju4.jpg', 76568, 'image/jpeg', NULL, NULL, 'pictures3', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 49, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(50, '5f3d24118c856743723975.jpg', 'parmaju3.jpg', 87745, 'image/jpeg', NULL, NULL, 'pictures3', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 50, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(51, '5f3d24118c9f7531207703.jpg', 'parmaju4.jpg', 76568, 'image/jpeg', NULL, NULL, 'pictures3', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 51, '2020-08-19 11:07:29', '2020-08-19 11:07:29'),
(60, '5f3e38a95f3e7989564301.jpg', 'M&S_Glinki_Petersburg_grey_30.jpg', 8849255, 'image/jpeg', 'Консьерж-служба', 'К услугам резидентов обслуживание по системе закрытого клуба 24/7.', 'pictures1', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 60, '2020-08-20 06:47:37', '2020-08-20 06:53:38'),
(61, '5f3e38a95ef4d522050372.jpg', 'M&S_Glinki_elevator_3.jpg', 12460316, 'image/jpeg', 'Персональные лифты', 'Для комфортного проживания резидентов предусмотрены индивидуальные лифты в резиденции из подземного паркинга.', 'pictures1', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 61, '2020-08-20 06:47:37', '2020-08-20 06:54:16'),
(63, '5f3e38ab560c9978687766.jpg', 'M&S_Glinki_Petersburg_spa_21.jpg', 7951089, 'image/jpeg', 'Wellness - центр', 'Одной из главных особенностей «Дома Балле» является Wellness – центр, представленный плавательным бассейном, сауной, хаммамом, массажным кабинетом, фитнес-баром, залом для парикмахера и стилиста и работающий по системе закрытого клуба – только для собственников резиденций.', 'pictures1', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 63, '2020-08-20 06:47:39', '2020-08-20 06:54:59'),
(64, '5f3e38ab5e806601687552.jpg', 'тренажерный зал.jpg', 245617, 'image/jpeg', 'Тренажерный зал', 'Зал для спортивных занятий оснащен тренажерами нового поколения премиум-класса, с возможностью проведения индивидуальных тренировок.', 'pictures1', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 64, '2020-08-20 06:47:39', '2020-08-20 06:55:22'),
(65, '5f3e3c877283a812455823.jpg', 'IMG_0246.JPG', 2192972, 'image/jpeg', NULL, NULL, 'pictures2', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 65, '2020-08-20 07:04:07', '2020-08-20 07:04:14'),
(66, '5f3e3c87e6511424340906.jpg', 'M&S_Glinki_Petersburg_grey_32.jpg', 6310889, 'image/jpeg', NULL, NULL, 'pictures2', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 66, '2020-08-20 07:04:07', '2020-08-20 07:04:14'),
(67, '5f3e3c88a8530386918601.jpg', 'Petersburg_13.JPG', 1881834, 'image/jpeg', NULL, NULL, 'pictures2', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 67, '2020-08-20 07:04:08', '2020-08-20 07:04:14'),
(68, '5f3e3c89bd523864406968.jpg', 'M&S_Glinki_Petersburg_spa_23.jpg', 11882586, 'image/jpeg', NULL, NULL, 'pictures2', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 68, '2020-08-20 07:04:09', '2020-08-20 07:04:14'),
(79, '5f3e451bc7595232385954.jpg', 'зал парикмахера.jpg', 952738, 'image/jpeg', NULL, NULL, 'picture', '3', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 79, '2020-08-20 07:40:43', '2020-08-20 07:40:46'),
(82, '5f3e4586a3ca8307657836.jpg', 'тренажерный зал_1.jpg', 549305, 'image/jpeg', NULL, NULL, 'picture', '4', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 82, '2020-08-20 07:42:30', '2020-08-20 07:42:32'),
(83, '5f3e45b26b085684846353.jpg', 'IMG_0246 (1).JPG', 2192972, 'image/jpeg', NULL, NULL, 'picture', '5', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 83, '2020-08-20 07:43:14', '2020-08-20 07:43:17'),
(84, '5f3e45efaa327305827978.jpg', 'Petersburg_14.JPG', 2080946, 'image/jpeg', NULL, NULL, 'picture', '6', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 84, '2020-08-20 07:44:15', '2020-08-20 07:44:17'),
(85, '5f3e462925925913536844.jpg', 'шубная.jpg', 116460, 'image/jpeg', NULL, NULL, 'picture', '7', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 85, '2020-08-20 07:45:13', '2020-08-20 07:45:14'),
(86, '5f3e46616e19e840233052.jpg', 'кладовые.jpg', 225023, 'image/jpeg', NULL, NULL, 'picture', '8', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 86, '2020-08-20 07:46:09', '2020-08-20 07:46:11'),
(87, '5f3e46c0a4cd9883626279.jpg', '57 консьерж.jpg', 274749, 'image/jpeg', NULL, NULL, 'picture', '1', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 87, '2020-08-20 07:47:44', '2020-08-20 07:47:46'),
(88, '5f3e46d66809a190493604.jpg', 'M&S_Glinki_Petersburg_spa_23.jpg', 11882586, 'image/jpeg', NULL, NULL, 'picture', '2', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 88, '2020-08-20 07:48:06', '2020-08-20 07:48:23'),
(89, '5f3e49b2c8790568023584.jpg', 'M&S_Glinki_Petersburg_grey_29.jpg', 5936361, 'image/jpeg', NULL, NULL, 'picture1', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 89, '2020-08-20 08:00:18', '2020-08-20 08:00:45'),
(90, '5f3e49c657ea7342131263.jpg', 'M&S_Glinki_Petersburg_grey_55.jpg', 7178826, 'image/jpeg', NULL, NULL, 'picture2', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 90, '2020-08-20 08:00:38', '2020-08-20 08:00:45'),
(91, '5f3e4a77c9100869877121.jpg', 'IMG_1302.JPG', 583592, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 91, '2020-08-20 08:03:35', '2020-08-20 08:03:45'),
(92, '5f3e4a7827974123930988.jpg', 'M&S_Glinki_Petersburg_grey_31.jpg', 5954446, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 92, '2020-08-20 08:03:36', '2020-08-20 08:03:45'),
(93, '5f3e4a78e10db436466649.jpg', 'M&S_Glinki_Petersburg_grey_52.jpg', 6514870, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 93, '2020-08-20 08:03:36', '2020-08-20 08:03:45'),
(94, '5f3e4a7a9ca1d437221118.jpg', 'M&S_Glinki_Petersburg_spa_25.jpg', 9346682, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 94, '2020-08-20 08:03:38', '2020-08-20 08:03:45'),
(95, '5f3e4a7ac63fd509076121.jpg', 'M&S_Glinki_Petersburg_spa_21 (1).jpg', 7951089, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 95, '2020-08-20 08:03:38', '2020-08-20 08:03:45'),
(96, '5f3e4a7dd8a28687778182.jpg', 'M&S_Glinki_Petersburg_spa_8.jpg', 8387201, 'image/jpeg', NULL, NULL, 'pictures', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 96, '2020-08-20 08:03:41', '2020-08-20 08:03:45'),
(98, '5f3e900e8c203445548912.jpg', 'паркинг.jpg', 407143, 'image/jpeg', NULL, NULL, 'picture', '10', 'Bboxdigi\\Bale\\Models\\InfrastructurePoint', 1, 98, '2020-08-20 13:00:30', '2020-08-20 13:00:33'),
(102, '5f3fa10f96c42269082507.pdf', 'BalleNamsPlani1L.pdf', 3419890, 'application/pdf', NULL, NULL, 'file', '1', 'Bboxdigi\\Bale\\Models\\Flat', 1, 102, '2020-08-21 08:25:19', '2020-08-21 08:26:25'),
(103, '5f3fa1953b029071772230.pdf', 'BalleNamsPlani2L.pdf', 3311573, 'application/pdf', NULL, NULL, 'file', '2', 'Bboxdigi\\Bale\\Models\\Flat', 1, 103, '2020-08-21 08:27:33', '2020-08-21 08:27:42'),
(104, '5f3fa6d6686dc986890502.svg', 'footer_img.svg', 44714, 'image/svg+xml', NULL, NULL, 'picture', '1', 'Bboxdigi\\Bale\\Models\\Footer', 1, 104, '2020-08-21 08:49:58', '2020-08-21 08:50:01'),
(108, '5f3fd0b443575787005191.png', 'plans12.png', 129909, 'image/png', NULL, NULL, 'plan_image', '2', 'Bboxdigi\\Bale\\Models\\Flat', 1, 108, '2020-08-21 11:48:36', '2020-08-21 11:48:38'),
(109, '5f3fd0bec6c3c356176130.png', 'plans11.png', 195841, 'image/png', NULL, NULL, 'plan_image', '1', 'Bboxdigi\\Bale\\Models\\Flat', 1, 109, '2020-08-21 11:48:46', '2020-08-21 11:48:49'),
(110, '5f3fd0cba793e869041978.png', 'plans13.png', 170915, 'image/png', NULL, NULL, 'plan_image', '3', 'Bboxdigi\\Bale\\Models\\Flat', 1, 110, '2020-08-21 11:48:59', '2020-08-21 11:49:01'),
(111, '5f3fd123382c5588892384.png', 'plans21.png', 181221, 'image/png', NULL, NULL, 'plan_image', '5', 'Bboxdigi\\Bale\\Models\\Flat', 1, 111, '2020-08-21 11:50:27', '2020-08-21 11:50:29'),
(112, '5f3fd128ef2b7172060204.png', 'plans22.png', 152586, 'image/png', NULL, NULL, 'plan_image', '6', 'Bboxdigi\\Bale\\Models\\Flat', 1, 112, '2020-08-21 11:50:32', '2020-08-21 11:50:34'),
(113, '5f3fd12daa129061057778.png', 'plans23.png', 173225, 'image/png', NULL, NULL, 'plan_image', '7', 'Bboxdigi\\Bale\\Models\\Flat', 1, 113, '2020-08-21 11:50:37', '2020-08-21 11:50:38'),
(114, '5f3fd13187c48481794515.png', 'plans24.png', 153980, 'image/png', NULL, NULL, 'plan_image', '8', 'Bboxdigi\\Bale\\Models\\Flat', 1, 114, '2020-08-21 11:50:41', '2020-08-21 11:50:43'),
(115, '5f3fd14e35326392473096.png', 'plans26.png', 201100, 'image/png', NULL, NULL, 'plan_image', '17', 'Bboxdigi\\Bale\\Models\\Flat', 1, 115, '2020-08-21 11:51:10', '2020-08-21 11:51:19'),
(116, '5f3fd1f9038bf821847863.png', 'plans31.png', 203706, 'image/png', NULL, NULL, 'plan_image', '9', 'Bboxdigi\\Bale\\Models\\Flat', 1, 116, '2020-08-21 11:54:01', '2020-08-21 11:54:02'),
(117, '5f3fd1fdba03b600016169.png', 'plans32.png', 154217, 'image/png', NULL, NULL, 'plan_image', '10', 'Bboxdigi\\Bale\\Models\\Flat', 1, 117, '2020-08-21 11:54:05', '2020-08-21 11:54:07'),
(118, '5f3fd202a854f574553642.png', 'plans33.png', 179945, 'image/png', NULL, NULL, 'plan_image', '11', 'Bboxdigi\\Bale\\Models\\Flat', 1, 118, '2020-08-21 11:54:10', '2020-08-21 11:54:12'),
(119, '5f3fd207a9660526084717.png', 'plans34.png', 160579, 'image/png', NULL, NULL, 'plan_image', '12', 'Bboxdigi\\Bale\\Models\\Flat', 1, 119, '2020-08-21 11:54:15', '2020-08-21 11:54:16'),
(120, '5f3fd24017daa447612360.png', 'plans35.png', 172792, 'image/png', NULL, NULL, 'plan_image', '18', 'Bboxdigi\\Bale\\Models\\Flat', 1, 120, '2020-08-21 11:55:12', '2020-08-21 11:55:19'),
(121, '5f3fd24f0e66a581817302.png', 'plans36.png', 200323, 'image/png', NULL, NULL, 'plan_image', '19', 'Bboxdigi\\Bale\\Models\\Flat', 1, 121, '2020-08-21 11:55:27', '2020-08-21 11:56:07'),
(122, '5f3fd28fd8860825322236.png', 'plans41.png', 199679, 'image/png', NULL, NULL, 'plan_image', '13', 'Bboxdigi\\Bale\\Models\\Flat', 1, 122, '2020-08-21 11:56:31', '2020-08-21 11:56:35'),
(123, '5f3fd2965ec66385163906.png', 'plans42.png', 158244, 'image/png', NULL, NULL, 'plan_image', '14', 'Bboxdigi\\Bale\\Models\\Flat', 1, 123, '2020-08-21 11:56:38', '2020-08-21 11:56:39'),
(124, '5f3fd29a47f97728306363.png', 'plans43.png', 177579, 'image/png', NULL, NULL, 'plan_image', '15', 'Bboxdigi\\Bale\\Models\\Flat', 1, 124, '2020-08-21 11:56:42', '2020-08-21 11:56:43'),
(125, '5f3fd29e1b675331199726.png', 'plans44.png', 161438, 'image/png', NULL, NULL, 'plan_image', '16', 'Bboxdigi\\Bale\\Models\\Flat', 1, 125, '2020-08-21 11:56:46', '2020-08-21 11:56:47'),
(126, '5f3fd2d6d85cb895354782.png', 'plans45.png', 187020, 'image/png', NULL, NULL, 'plan_image', '20', 'Bboxdigi\\Bale\\Models\\Flat', 1, 126, '2020-08-21 11:57:42', '2020-08-21 11:57:53'),
(127, '5f3fd3c21b4d5286761520.png', 'plans46.png', 198796, 'image/png', NULL, NULL, 'plan_image', '21', 'Bboxdigi\\Bale\\Models\\Flat', 1, 127, '2020-08-21 12:01:38', '2020-08-21 12:01:50'),
(128, '5f3fd3ebb74f3176105001.png', 'plans51.png', 200160, 'image/png', NULL, NULL, 'plan_image', '22', 'Bboxdigi\\Bale\\Models\\Flat', 1, 128, '2020-08-21 12:02:19', '2020-08-21 12:03:00'),
(129, '5f3fd43c445f6572866199.png', 'plans52.png', 178149, 'image/png', NULL, NULL, 'plan_image', '23', 'Bboxdigi\\Bale\\Models\\Flat', 1, 129, '2020-08-21 12:03:40', '2020-08-21 12:03:41'),
(130, '5f3fd4c16c162528005396.png', 'plans53.png', 169580, 'image/png', NULL, NULL, 'plan_image', '24', 'Bboxdigi\\Bale\\Models\\Flat', 1, 130, '2020-08-21 12:05:53', '2020-08-21 12:05:55'),
(131, '5f3fd4c7e9ab3896982534.png', 'plans54.png', 156311, 'image/png', NULL, NULL, 'plan_image', '25', 'Bboxdigi\\Bale\\Models\\Flat', 1, 131, '2020-08-21 12:05:59', '2020-08-21 12:06:30'),
(132, '5f3fd52a90316623655971.png', 'plans55.png', 189717, 'image/png', NULL, NULL, 'plan_image', '26', 'Bboxdigi\\Bale\\Models\\Flat', 1, 132, '2020-08-21 12:07:38', '2020-08-21 12:07:40'),
(133, '5f3fd53ad6237415711070.png', 'plans56.png', 201930, 'image/png', NULL, NULL, 'plan_image', '27', 'Bboxdigi\\Bale\\Models\\Flat', 1, 133, '2020-08-21 12:07:54', '2020-08-21 12:08:26'),
(139, '5f43c7951c1a5912261864.png', '7_Tchaikovsky.png', 68416, 'image/png', NULL, NULL, 'picture', '5', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 139, '2020-08-24 11:58:45', '2020-08-24 11:58:47'),
(140, '5f43c7a986e59818913612.png', '2_Gogol.png', 65418, 'image/png', NULL, NULL, 'picture', '2', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 140, '2020-08-24 11:59:05', '2020-08-24 11:59:07'),
(141, '5f43c7b853855624883331.png', '6_Musorgskiy.png', 87517, 'image/png', NULL, NULL, 'picture', '3', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 141, '2020-08-24 11:59:20', '2020-08-24 11:59:22'),
(142, '5f43c7ca604fc254536646.png', '1_Lermontov.png', 88340, 'image/png', NULL, NULL, 'picture', '1', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 142, '2020-08-24 11:59:38', '2020-08-24 11:59:41'),
(143, '5f43c7db1a0e4994272969.png', '4_Glinka.png', 85024, 'image/png', NULL, NULL, 'picture', '6', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 143, '2020-08-24 11:59:55', '2020-08-24 11:59:56'),
(144, '5f43c7eca1ce7956116259.png', '8_Serov.png', 71479, 'image/png', NULL, NULL, 'picture', '7', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 144, '2020-08-24 12:00:12', '2020-08-24 12:00:14'),
(145, '5f43c7fbf1c8e146314923.png', '5_Pushkin.png', 76602, 'image/png', NULL, NULL, 'picture', '8', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 145, '2020-08-24 12:00:27', '2020-08-24 12:00:30'),
(146, '5f43c80d41fd6039297216.png', '3_Blok.png', 72743, 'image/png', NULL, NULL, 'picture', '4', 'Bboxdigi\\Bale\\Models\\Neighbor', 1, 146, '2020-08-24 12:00:45', '2020-08-24 12:00:47'),
(149, '5f44d8d960608237691328.jpg', '02.jpg', 1623082, 'image/jpeg', NULL, NULL, 'overview_picture', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 149, '2020-08-25 07:24:41', '2020-08-25 07:24:48'),
(150, '5f44d95f1a35a945423149.jpg', '01.jpg', 1834086, 'image/jpeg', 'Изысканные интерьеры', 'Отделка внутренних интерьеров дома выполнена в стиле ар-деко из благородных натуральных материалов. Особую роскошь интерьерам добавляют предметы искусства.', NULL, NULL, NULL, 1, 150, '2020-08-25 07:26:55', '2020-08-25 07:27:22'),
(151, '5f44da43dfc5e323909819.jpg', '01.jpg', 1834086, 'image/jpeg', 'Изысканные интерьеры', 'Отделка внутренних интерьеров дома выполнена в стиле ар-деко из благородных натуральных материалов. Особую роскошь интерьерам добавляют предметы искусства.', NULL, NULL, NULL, 1, 151, '2020-08-25 07:30:43', '2020-08-25 07:31:09'),
(152, '5f44dc869e632390853560.jpg', '01.jpg', 1834086, 'image/jpeg', 'Изысканные интерьеры', 'Отделка внутренних интерьеров дома выполнена в стиле ар-деко из благородных натуральных материалов. Особую роскошь интерьерам добавляют предметы искусства.', NULL, NULL, NULL, 1, 152, '2020-08-25 07:40:22', '2020-08-25 07:40:51'),
(153, '5f44dd6ae9ff9541440166.jpg', '01.jpg', 1834086, 'image/jpeg', 'Изысканные интерьеры', 'Отделка внутренних интерьеров дома выполнена в стиле ар-деко из благородных натуральных материалов. Особую роскошь интерьерам добавляют предметы искусства.', 'pictures1', '1', 'Bboxdigi\\Bale\\Models\\About', 1, 153, '2020-08-25 07:44:10', '2020-08-25 07:51:21'),
(154, '5f4518f0335fa172807141.png', 'floor1.png', 188689, 'image/png', NULL, NULL, 'plan_image', '1', 'Bboxdigi\\Bale\\Models\\Floor', 1, 154, '2020-08-25 11:58:08', '2020-08-25 11:58:12'),
(155, '5f451a9099eea488780411.png', 'maja11.png', 178734, 'image/png', NULL, NULL, 'building_image', '1', 'Bboxdigi\\Bale\\Models\\Flat', 1, 155, '2020-08-25 12:05:04', '2020-08-25 12:05:07'),
(156, '5f451ad8239b8384269024.png', 'maja12.png', 179121, 'image/png', NULL, NULL, 'building_image', '2', 'Bboxdigi\\Bale\\Models\\Flat', 1, 156, '2020-08-25 12:06:16', '2020-08-25 12:06:18'),
(157, '5f451afa4b3a5276101889.png', 'maja13.png', 220490, 'image/png', NULL, NULL, 'building_image', '3', 'Bboxdigi\\Bale\\Models\\Flat', 1, 157, '2020-08-25 12:06:50', '2020-08-25 12:06:56'),
(158, '5f451c2149611960702826.png', 'floor2.png', 189852, 'image/png', NULL, NULL, 'plan_image', '2', 'Bboxdigi\\Bale\\Models\\Floor', 1, 158, '2020-08-25 12:11:45', '2020-08-25 12:11:47'),
(159, '5f451c4aa2201081600356.png', 'maja21.png', 180348, 'image/png', NULL, NULL, 'building_image', '5', 'Bboxdigi\\Bale\\Models\\Flat', 1, 159, '2020-08-25 12:12:26', '2020-08-25 12:12:28'),
(160, '5f451c5d3caf8640834831.png', 'maja22.png', 178681, 'image/png', NULL, NULL, 'building_image', '6', 'Bboxdigi\\Bale\\Models\\Flat', 1, 160, '2020-08-25 12:12:45', '2020-08-25 12:12:56'),
(161, '5f451c72e0849007249147.png', 'maja23.png', 166126, 'image/png', NULL, NULL, 'building_image', '7', 'Bboxdigi\\Bale\\Models\\Flat', 1, 161, '2020-08-25 12:13:06', '2020-08-25 12:13:09'),
(162, '5f451c7b1819d130534480.png', 'maja24.png', 166087, 'image/png', NULL, NULL, 'building_image', '8', 'Bboxdigi\\Bale\\Models\\Flat', 1, 162, '2020-08-25 12:13:15', '2020-08-25 12:13:22'),
(163, '5f451c87a2bd5046892599.png', 'maja26.png', 220924, 'image/png', NULL, NULL, 'building_image', '17', 'Bboxdigi\\Bale\\Models\\Flat', 1, 163, '2020-08-25 12:13:27', '2020-08-25 12:13:33'),
(164, '5f451cabb9cee480093963.png', 'floor3.png', 187348, 'image/png', NULL, NULL, 'plan_image', '3', 'Bboxdigi\\Bale\\Models\\Floor', 1, 164, '2020-08-25 12:14:03', '2020-08-25 12:14:08'),
(165, '5f451cd77326f401585830.png', 'maja31.png', 179901, 'image/png', NULL, NULL, 'building_image', '9', 'Bboxdigi\\Bale\\Models\\Flat', 1, 165, '2020-08-25 12:14:47', '2020-08-25 12:14:49'),
(166, '5f451ce22ede4101135625.png', 'maja32.png', 178385, 'image/png', NULL, NULL, 'building_image', '10', 'Bboxdigi\\Bale\\Models\\Flat', 1, 166, '2020-08-25 12:14:58', '2020-08-25 12:15:01'),
(167, '5f451cebc261d380731227.png', 'maja33.png', 166196, 'image/png', NULL, NULL, 'building_image', '11', 'Bboxdigi\\Bale\\Models\\Flat', 1, 167, '2020-08-25 12:15:07', '2020-08-25 12:15:09'),
(168, '5f451cf3c8491654269511.png', 'maja34.png', 166076, 'image/png', NULL, NULL, 'building_image', '12', 'Bboxdigi\\Bale\\Models\\Flat', 1, 168, '2020-08-25 12:15:15', '2020-08-25 12:15:17'),
(169, '5f451cfc7cb92548049166.png', 'maja35.png', 219444, 'image/png', NULL, NULL, 'building_image', '18', 'Bboxdigi\\Bale\\Models\\Flat', 1, 169, '2020-08-25 12:15:24', '2020-08-25 12:15:25'),
(170, '5f451d0429824211755517.png', 'maja36.png', 221262, 'image/png', NULL, NULL, 'building_image', '19', 'Bboxdigi\\Bale\\Models\\Flat', 1, 170, '2020-08-25 12:15:32', '2020-08-25 12:15:34'),
(171, '5f451d1624694613457410.png', 'floor4.png', 175018, 'image/png', NULL, NULL, 'plan_image', '4', 'Bboxdigi\\Bale\\Models\\Floor', 1, 171, '2020-08-25 12:15:50', '2020-08-25 12:15:51'),
(172, '5f451d421b165048972861.png', 'maja41.png', 179789, 'image/png', NULL, NULL, 'building_image', '13', 'Bboxdigi\\Bale\\Models\\Flat', 1, 172, '2020-08-25 12:16:34', '2020-08-25 12:16:36'),
(175, '5f451d4b1feb6727768805.png', 'maja42.png', 178162, 'image/png', NULL, NULL, 'building_image', '14', 'Bboxdigi\\Bale\\Models\\Flat', 1, 175, '2020-08-25 12:16:43', '2020-08-25 12:16:44'),
(176, '5f451d4fee890199112839.png', 'maja43.png', 166107, 'image/png', NULL, NULL, 'building_image', '15', 'Bboxdigi\\Bale\\Models\\Flat', 1, 176, '2020-08-25 12:16:47', '2020-08-25 12:16:53'),
(177, '5f451d591ee79287228736.png', 'maja44.png', 165970, 'image/png', NULL, NULL, 'building_image', '16', 'Bboxdigi\\Bale\\Models\\Flat', 1, 177, '2020-08-25 12:16:57', '2020-08-25 12:17:02'),
(178, '5f451d659738c577825145.png', 'maja45.png', 219245, 'image/png', NULL, NULL, 'building_image', '20', 'Bboxdigi\\Bale\\Models\\Flat', 1, 178, '2020-08-25 12:17:09', '2020-08-25 12:17:10'),
(179, '5f451d6e248f3462344506.png', 'maja46.png', 220977, 'image/png', NULL, NULL, 'building_image', '21', 'Bboxdigi\\Bale\\Models\\Flat', 1, 179, '2020-08-25 12:17:18', '2020-08-25 12:17:19'),
(180, '5f451d7e40f38889114773.png', 'floor5.png', 181080, 'image/png', NULL, NULL, 'plan_image', '5', 'Bboxdigi\\Bale\\Models\\Floor', 1, 180, '2020-08-25 12:17:34', '2020-08-25 12:18:02'),
(181, '5f451da4358a5711830910.png', 'maja51.png', 179158, 'image/png', NULL, NULL, 'building_image', '22', 'Bboxdigi\\Bale\\Models\\Flat', 1, 181, '2020-08-25 12:18:12', '2020-08-25 12:18:13'),
(182, '5f451daa0c979493261703.png', 'maja52.png', 177929, 'image/png', NULL, NULL, 'building_image', '23', 'Bboxdigi\\Bale\\Models\\Flat', 1, 182, '2020-08-25 12:18:18', '2020-08-25 12:18:23'),
(183, '5f451db79460f426197663.png', 'maja53.png', 166090, 'image/png', NULL, NULL, 'building_image', '24', 'Bboxdigi\\Bale\\Models\\Flat', 1, 183, '2020-08-25 12:18:31', '2020-08-25 12:18:33'),
(184, '5f451dbfdeaca643037135.png', 'maja54.png', 166102, 'image/png', NULL, NULL, 'building_image', '25', 'Bboxdigi\\Bale\\Models\\Flat', 1, 184, '2020-08-25 12:18:39', '2020-08-25 12:18:41'),
(185, '5f451dcd8e944379779774.png', 'maja55.png', 219304, 'image/png', NULL, NULL, 'building_image', '26', 'Bboxdigi\\Bale\\Models\\Flat', 1, 185, '2020-08-25 12:18:53', '2020-08-25 12:18:55'),
(186, '5f451dd5556d4552672503.png', 'maja56.png', 220625, 'image/png', NULL, NULL, 'building_image', '27', 'Bboxdigi\\Bale\\Models\\Flat', 1, 186, '2020-08-25 12:19:01', '2020-08-25 12:19:02'),
(187, '5f451e5c902d1116466559.png', 'Krassky.png', 24755, 'image/png', 'Krassky', 'Основанная в Латвии международная компания – KRASSKY – принимала участие в работе над отделкой Дома Балле. Клиентами KRASSKY являются люди с высокими требованиями к сервису и богатым жизненным опытом. Мы считаем, что хороший интерьер подчеркивает личность и стиль жизни е го пользователя, поэтому для каждого клиента создаются индивидуальные решения. Мы верим, что хороший дизайн улучшает качество жизни, долговечен и функционален.\r\n\r\n119021, Москва, ул. Тимура Фрунзе 11/1\r\n +7 (495) 181-53-00\r\n krassky@krassky.ru\r\n www.krassky.ru', 'pictures_2', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 187, '2020-08-25 12:21:16', '2020-08-25 12:23:37'),
(188, '5f451e6487874392323331.png', 'Morozov.png', 25412, 'image/png', 'Morozov & Son', 'Мы сделали мебель для общих помещений Дома Балле, в котором вы намереваетесь приобрести апартаменты. Рассказ о себе будет очень коротким: лучше один раз увидеть. На этих фото 1/125 000 часть того, что мы спроектировали, изготовили и смонтировали за 25 лет на 5 континентах. Позвоните, пожалуйста, мне – и у вас тоже будет идеальный интерьер “под ключ”. \r\nP.S. Просто у нас нет конкурентов.\r\n\r\nУл. Клейсту 30, Рига, LV-1067, Латвия\r\n(+371) 67869850\r\n info@morozovandson.com\r\n www.morozovandson.com', 'pictures_2', '1', 'Bboxdigi\\Bale\\Models\\Interrior', 1, 188, '2020-08-25 12:21:24', '2020-08-25 12:23:37'),
(189, '5f461801efde8538857666.pdf', 'BalleNamsPlani3L.pdf', 3210444, 'application/pdf', NULL, NULL, 'file', '3', 'Bboxdigi\\Bale\\Models\\Flat', 1, 189, '2020-08-26 06:06:25', '2020-08-26 06:06:37'),
(190, '5f46472dc467f982075126.pdf', 'BalleNamsPlani21.pdf', 3828632, 'application/pdf', NULL, NULL, 'file', '5', 'Bboxdigi\\Bale\\Models\\Flat', 1, 190, '2020-08-26 09:27:41', '2020-08-26 09:27:46'),
(191, '5f46481dbab6a115211862.pdf', 'BalleNamsPlani23.pdf', 4706986, 'application/pdf', NULL, NULL, 'file', '7', 'Bboxdigi\\Bale\\Models\\Flat', 1, 191, '2020-08-26 09:31:41', '2020-08-26 09:32:03'),
(192, '5f464884e96cb180763927.pdf', 'BalleNamsPlani24.pdf', 5258193, 'application/pdf', NULL, NULL, 'file', '8', 'Bboxdigi\\Bale\\Models\\Flat', 1, 192, '2020-08-26 09:33:24', '2020-08-26 09:33:29'),
(193, '5f4648e0e4662327025316.pdf', 'BalleNamsPlani26.pdf', 4019042, 'application/pdf', NULL, NULL, 'file', '17', 'Bboxdigi\\Bale\\Models\\Flat', 1, 193, '2020-08-26 09:34:56', '2020-08-26 09:35:00'),
(194, '5f464969d80dc641602551.pdf', 'BalleNamsPlani31.pdf', 3995117, 'application/pdf', NULL, NULL, 'file', '9', 'Bboxdigi\\Bale\\Models\\Flat', 1, 194, '2020-08-26 09:37:13', '2020-08-26 09:37:18'),
(195, '5f4649aac8ab0515465464.pdf', 'BalleNamsPlani32.pdf', 3691437, 'application/pdf', NULL, NULL, 'file', '10', 'Bboxdigi\\Bale\\Models\\Flat', 1, 195, '2020-08-26 09:38:18', '2020-08-26 09:38:55'),
(196, '5f464a22e1db9683111676.pdf', 'BalleNamsPlani33.pdf', 4749916, 'application/pdf', NULL, NULL, 'file', '11', 'Bboxdigi\\Bale\\Models\\Flat', 1, 196, '2020-08-26 09:40:18', '2020-08-26 09:40:42'),
(197, '5f464a97102c6815814297.pdf', 'BalleNamsPlani34.pdf', 4894902, 'application/pdf', NULL, NULL, 'file', '12', 'Bboxdigi\\Bale\\Models\\Flat', 1, 197, '2020-08-26 09:42:15', '2020-08-26 09:42:32'),
(198, '5f464aeb786ae199444984.pdf', 'BalleNamsPlani35.pdf', 3944756, 'application/pdf', NULL, NULL, 'file', '18', 'Bboxdigi\\Bale\\Models\\Flat', 1, 198, '2020-08-26 09:43:39', '2020-08-26 09:43:44'),
(199, '5f464b1e4a309459105937.pdf', 'BalleNamsPlani36.pdf', 3979796, 'application/pdf', NULL, NULL, 'file', '19', 'Bboxdigi\\Bale\\Models\\Flat', 1, 199, '2020-08-26 09:44:30', '2020-08-26 09:44:37'),
(200, '5f464fd9a3e6e359153445.pdf', 'BalleNamsPlani41.pdf', 4151807, 'application/pdf', NULL, NULL, 'file', '13', 'Bboxdigi\\Bale\\Models\\Flat', 1, 200, '2020-08-26 10:04:41', '2020-08-26 10:04:45'),
(201, '5f4653093810d010028005.pdf', 'BalleNamsPlani42.pdf', 3978750, 'application/pdf', NULL, NULL, 'file', '14', 'Bboxdigi\\Bale\\Models\\Flat', 1, 201, '2020-08-26 10:18:17', '2020-08-26 10:18:20'),
(202, '5f46535f1b7fd074200458.pdf', 'BalleNamsPlani43.pdf', 4925901, 'application/pdf', NULL, NULL, 'file', '15', 'Bboxdigi\\Bale\\Models\\Flat', 1, 202, '2020-08-26 10:19:43', '2020-08-26 10:19:46'),
(203, '5f46544d0a80f581241628.pdf', 'BalleNamsPlani44.pdf', 5030551, 'application/pdf', NULL, NULL, 'file', '16', 'Bboxdigi\\Bale\\Models\\Flat', 1, 203, '2020-08-26 10:23:41', '2020-08-26 10:23:45'),
(204, '5f4654900c556683040788.pdf', 'BalleNamsPlani45.pdf', 4272260, 'application/pdf', NULL, NULL, 'file', '20', 'Bboxdigi\\Bale\\Models\\Flat', 1, 204, '2020-08-26 10:24:48', '2020-08-26 10:24:54'),
(205, '5f46563822710060738166.pdf', 'BalleNamsPlani46.pdf', 4254361, 'application/pdf', NULL, NULL, 'file', '21', 'Bboxdigi\\Bale\\Models\\Flat', 1, 205, '2020-08-26 10:31:52', '2020-08-26 10:32:56'),
(206, '5f46591097ea5747706030.pdf', 'BalleNamsPlani51.pdf', 3776231, 'application/pdf', NULL, NULL, 'file', '22', 'Bboxdigi\\Bale\\Models\\Flat', 1, 206, '2020-08-26 10:44:00', '2020-08-26 10:44:04'),
(207, '5f46594d45ea1276284539.pdf', 'BalleNamsPlani52.pdf', 3695748, 'application/pdf', NULL, NULL, 'file', '23', 'Bboxdigi\\Bale\\Models\\Flat', 1, 207, '2020-08-26 10:45:01', '2020-08-26 10:45:06'),
(208, '5f4659ed4af93307563514.pdf', 'BalleNamsPlani53.pdf', 4740881, 'application/pdf', NULL, NULL, 'file', '24', 'Bboxdigi\\Bale\\Models\\Flat', 1, 208, '2020-08-26 10:47:41', '2020-08-26 10:47:44'),
(209, '5f465a19974ba896852218.pdf', 'BalleNamsPlani54.pdf', 4820719, 'application/pdf', NULL, NULL, 'file', '25', 'Bboxdigi\\Bale\\Models\\Flat', 1, 209, '2020-08-26 10:48:25', '2020-08-26 10:48:28'),
(210, '5f465a3fe8d4e257645423.pdf', 'BalleNamsPlani55.pdf', 3817884, 'application/pdf', NULL, NULL, 'file', '26', 'Bboxdigi\\Bale\\Models\\Flat', 1, 210, '2020-08-26 10:49:03', '2020-08-26 10:49:06'),
(211, '5f465a65ce480010959625.pdf', 'BalleNamsPlani56.pdf', 3840090, 'application/pdf', NULL, NULL, 'file', '27', 'Bboxdigi\\Bale\\Models\\Flat', 1, 211, '2020-08-26 10:49:41', '2020-08-26 10:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2020-08-19 11:07:30', '2020-08-19 11:07:30'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2020-08-19 11:07:30', '2020-08-19 11:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'system', 'update', 'retry', '1603782468');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'Bboxdigi.Bale', 'comment', '1.0.1', 'First version of Bale', '2020-08-19 11:07:28'),
(2, 'Bboxdigi.Bale', 'script', '1.0.2', 'create_index_pages_table.php', '2020-08-19 11:07:28'),
(3, 'Bboxdigi.Bale', 'comment', '1.0.2', 'Create index_pages table', '2020-08-19 11:07:28'),
(4, 'Bboxdigi.Bale', 'script', '1.0.3', 'create_headers_table.php', '2020-08-19 11:07:29'),
(5, 'Bboxdigi.Bale', 'comment', '1.0.3', 'Create headers table', '2020-08-19 11:07:29'),
(6, 'Bboxdigi.Bale', 'script', '1.0.4', 'create_footers_table.php', '2020-08-19 11:07:29'),
(7, 'Bboxdigi.Bale', 'comment', '1.0.4', 'Create footers table', '2020-08-19 11:07:29'),
(8, 'Bboxdigi.Bale', 'script', '1.0.5', 'create_location_categories_table.php', '2020-08-19 11:07:29'),
(9, 'Bboxdigi.Bale', 'comment', '1.0.5', 'Create location_categories table', '2020-08-19 11:07:29'),
(10, 'Bboxdigi.Bale', 'script', '1.0.6', 'create_locations_table.php', '2020-08-19 11:07:29'),
(11, 'Bboxdigi.Bale', 'comment', '1.0.6', 'Create locations table', '2020-08-19 11:07:29'),
(12, 'Bboxdigi.Bale', 'script', '1.0.7', 'create_infrastructures_table.php', '2020-08-19 11:07:29'),
(13, 'Bboxdigi.Bale', 'comment', '1.0.7', 'Create infrastructures table', '2020-08-19 11:07:29'),
(14, 'Bboxdigi.Bale', 'script', '1.0.8', 'create_infrastructure_points_table.php', '2020-08-19 11:07:29'),
(15, 'Bboxdigi.Bale', 'comment', '1.0.8', 'Create infrastructure_points table', '2020-08-19 11:07:29'),
(16, 'Bboxdigi.Bale', 'script', '1.0.9', 'create_infrastructure_solutions_table.php', '2020-08-19 11:07:29'),
(17, 'Bboxdigi.Bale', 'comment', '1.0.9', 'Create infrastructure_solutions table', '2020-08-19 11:07:29'),
(18, 'Bboxdigi.Bale', 'script', '1.0.10', 'create_interriors_table.php', '2020-08-19 11:07:29'),
(19, 'Bboxdigi.Bale', 'comment', '1.0.10', 'Create interriors table', '2020-08-19 11:07:29'),
(20, 'Bboxdigi.Bale', 'script', '1.0.11', 'create_floors_table.php', '2020-08-19 11:07:29'),
(21, 'Bboxdigi.Bale', 'comment', '1.0.11', 'Create floors table', '2020-08-19 11:07:29'),
(22, 'Bboxdigi.Bale', 'script', '1.0.12', 'create_flats_table.php', '2020-08-19 11:07:29'),
(23, 'Bboxdigi.Bale', 'comment', '1.0.12', 'Create flats table', '2020-08-19 11:07:29'),
(24, 'Bboxdigi.Bale', 'script', '1.0.13', 'create_contacts_table.php', '2020-08-19 11:07:29'),
(25, 'Bboxdigi.Bale', 'comment', '1.0.13', 'Create contacts table', '2020-08-19 11:07:29'),
(26, 'Bboxdigi.Bale', 'script', '1.0.14', 'create_abouts_table.php', '2020-08-19 11:07:29'),
(27, 'Bboxdigi.Bale', 'comment', '1.0.14', 'Create abouts table', '2020-08-19 11:07:29'),
(28, 'Bboxdigi.Bale', 'script', '1.0.15', 'create_neighbors_table.php', '2020-08-19 11:07:29'),
(29, 'Bboxdigi.Bale', 'comment', '1.0.15', 'Create neighbors table', '2020-08-19 11:07:29'),
(30, 'Indikator.Backend', 'comment', '1.0.0', 'First version of Backend Plus.', '2020-08-19 11:07:29'),
(31, 'Indikator.Backend', 'comment', '1.0.1', 'Fixed the update count issue.', '2020-08-19 11:07:29'),
(32, 'Indikator.Backend', 'comment', '1.0.2', 'Added Last logins widget.', '2020-08-19 11:07:29'),
(33, 'Indikator.Backend', 'comment', '1.0.3', 'Added RSS viewer widget.', '2020-08-19 11:07:29'),
(34, 'Indikator.Backend', 'comment', '1.0.4', 'Minor improvements and bugfix.', '2020-08-19 11:07:29'),
(35, 'Indikator.Backend', 'comment', '1.0.5', 'Added Random images widget.', '2020-08-19 11:07:29'),
(36, 'Indikator.Backend', 'comment', '1.0.6', 'Added virtual keyboard option.', '2020-08-19 11:07:29'),
(37, 'Indikator.Backend', 'comment', '1.1.0', 'Added Lorem ipsum components (image and text).', '2020-08-19 11:07:29'),
(38, 'Indikator.Backend', 'comment', '1.1.0', 'Improving the Random images widget with slideshow.', '2020-08-19 11:07:29'),
(39, 'Indikator.Backend', 'comment', '1.1.0', 'Added Turkish translation (thanks to mahony0).', '2020-08-19 11:07:29'),
(40, 'Indikator.Backend', 'comment', '1.1.0', 'Fixed the URL path issue by virtual keyboard.', '2020-08-19 11:07:29'),
(41, 'Indikator.Backend', 'comment', '1.1.1', 'Hide the \"Find more themes\" link.', '2020-08-19 11:07:29'),
(42, 'Indikator.Backend', 'comment', '1.1.2', 'Added German translation.', '2020-08-19 11:07:29'),
(43, 'Indikator.Backend', 'comment', '1.1.3', 'The widgets work on localhost too.', '2020-08-19 11:07:29'),
(44, 'Indikator.Backend', 'comment', '1.1.4', 'Added Spanish translation.', '2020-08-19 11:07:29'),
(45, 'Indikator.Backend', 'comment', '1.2.0', 'All features are working on the whole backend.', '2020-08-19 11:07:29'),
(46, 'Indikator.Backend', 'comment', '1.2.1', 'Rounded profile image is optional in top menu.', '2020-08-19 11:07:29'),
(47, 'Indikator.Backend', 'comment', '1.2.2', 'Fixed the authenticated user bug.', '2020-08-19 11:07:29'),
(48, 'Indikator.Backend', 'comment', '1.2.3', 'Hide the Media menu optional in top menu.', '2020-08-19 11:07:29'),
(49, 'Indikator.Backend', 'comment', '1.2.4', 'Minor improvements and bugfix.', '2020-08-19 11:07:29'),
(50, 'Indikator.Backend', 'comment', '1.2.5', 'Renamed the name of backend widgets.', '2020-08-19 11:07:29'),
(51, 'Indikator.Backend', 'comment', '1.2.6', 'Improved the automatic search focus.', '2020-08-19 11:07:29'),
(52, 'Indikator.Backend', 'comment', '1.2.7', 'Minor improvements.', '2020-08-19 11:07:29'),
(53, 'Indikator.Backend', 'comment', '1.2.8', 'Fixed the hiding Media menu issue.', '2020-08-19 11:07:29'),
(54, 'Indikator.Backend', 'comment', '1.2.9', 'Improved the widget exception handling.', '2020-08-19 11:07:29'),
(55, 'Indikator.Backend', 'comment', '1.3.0', 'Added 2 new options for Settings.', '2020-08-19 11:07:29'),
(56, 'Indikator.Backend', 'comment', '1.3.1', 'Fixed the search field hide issue.', '2020-08-19 11:07:29'),
(57, 'Indikator.Backend', 'comment', '1.3.2', 'Delete only demo folder instead of october.', '2020-08-19 11:07:29'),
(58, 'Indikator.Backend', 'comment', '1.3.3', 'Added clear button option to form fields.', '2020-08-19 11:07:29'),
(59, 'Indikator.Backend', 'comment', '1.3.4', 'Improved the Media menu hiding.', '2020-08-19 11:07:29'),
(60, 'Indikator.Backend', 'comment', '1.3.5', 'Fixed the automatically focus option.', '2020-08-19 11:07:29'),
(61, 'Indikator.Backend', 'comment', '1.3.6', 'Added the Cache dashboard widget.', '2020-08-19 11:07:29'),
(62, 'Indikator.Backend', 'comment', '1.4.0', 'Added 2 new form widgets.', '2020-08-19 11:07:29'),
(63, 'Indikator.Backend', 'comment', '1.4.1', 'Added new colorpicker form widget.', '2020-08-19 11:07:29'),
(64, 'Indikator.Backend', 'comment', '1.4.2', 'Minor improvements.', '2020-08-19 11:07:29'),
(65, 'Indikator.Backend', 'comment', '1.4.3', 'Improved the Cache dashboard widget.', '2020-08-19 11:07:29'),
(66, 'Indikator.Backend', 'comment', '1.4.4', 'Updated for latest October.', '2020-08-19 11:07:29'),
(67, 'Indikator.Backend', 'comment', '1.4.5', 'Minor improvements and bugfix.', '2020-08-19 11:07:29'),
(68, 'Indikator.Backend', 'comment', '1.4.6', 'Improved the UI and fixed bug.', '2020-08-19 11:07:29'),
(69, 'Indikator.Backend', 'comment', '1.4.7', 'Hide the label in top menu.', '2020-08-19 11:07:29'),
(70, 'Indikator.Backend', 'comment', '1.4.8', 'Enable the gzip compression.', '2020-08-19 11:07:29'),
(71, 'Indikator.Backend', 'script', '1.5.0', 'create_trash_table.php', '2020-08-19 11:07:29'),
(72, 'Indikator.Backend', 'comment', '1.5.0', 'Delete the unused files and folders.', '2020-08-19 11:07:29'),
(73, 'Indikator.Backend', 'comment', '1.5.1', 'Minor improvements and bugfix.', '2020-08-19 11:07:29'),
(74, 'Indikator.Backend', 'comment', '1.5.2', 'Improved the Trash items page.', '2020-08-19 11:07:29'),
(75, 'Indikator.Backend', 'comment', '1.5.3', 'Expanded the Trash items page.', '2020-08-19 11:07:29'),
(76, 'Indikator.Backend', 'comment', '1.5.4', 'Minor improvements.', '2020-08-19 11:07:29'),
(77, 'Indikator.Backend', 'comment', '1.5.5', 'Added tooltip when hiding the labels.', '2020-08-19 11:07:29'),
(78, 'Indikator.Backend', 'comment', '1.5.6', 'Fixed the page overflow issue.', '2020-08-19 11:07:29'),
(79, 'Indikator.Backend', 'comment', '1.5.7', 'Added the context menu feature.', '2020-08-19 11:07:29'),
(80, 'Indikator.Backend', 'comment', '1.5.8', 'Improved the context menu.', '2020-08-19 11:07:29'),
(81, 'Indikator.Backend', 'comment', '1.6.0', 'Available the Elite version.', '2020-08-19 11:07:29'),
(82, 'Indikator.Backend', 'comment', '1.6.1', 'Added the Russian translation.', '2020-08-19 11:07:29'),
(83, 'Indikator.Backend', 'comment', '1.6.2', 'Added the Brazilian Portuguese lang.', '2020-08-19 11:07:29'),
(84, 'Indikator.Backend', 'comment', '1.6.3', 'Minor improvements.', '2020-08-19 11:07:29'),
(85, 'Indikator.Backend', 'comment', '1.6.4', 'Fixed the German translation.', '2020-08-19 11:07:29'),
(86, 'Indikator.Backend', 'comment', '1.6.5', 'Fixed the Cache widget issue.', '2020-08-19 11:07:29'),
(87, 'Indikator.Backend', 'comment', '1.6.6', '!!! Updated for October 420+.', '2020-08-19 11:07:29'),
(88, 'Indikator.Backend', 'comment', '1.6.7', 'Added more trash items.', '2020-08-19 11:07:29'),
(89, 'Indikator.Backend', 'comment', '1.6.8', 'Minor improvements.', '2020-08-19 11:07:29'),
(90, 'Indikator.Backend', 'comment', '1.6.9', 'Added permission to Dashboard widgets.', '2020-08-19 11:07:29'),
(91, 'Indikator.Backend', 'comment', '1.6.10', 'Added Polish translation.', '2020-08-19 11:07:29'),
(92, 'Indikator.Backend', 'comment', '1.6.11', 'Updated the trash file list.', '2020-08-19 11:07:29'),
(93, 'Indikator.Backend', 'comment', '1.6.12', 'Added more trash items.', '2020-08-19 11:07:29'),
(94, 'October.Drivers', 'comment', '1.0.1', 'First version of Drivers', '2020-08-19 11:07:29'),
(95, 'October.Drivers', 'comment', '1.0.2', 'Update Guzzle library to version 5.0', '2020-08-19 11:07:29'),
(96, 'October.Drivers', 'comment', '1.1.0', 'Update AWS library to version 3.0', '2020-08-19 11:07:29'),
(97, 'October.Drivers', 'comment', '1.1.1', 'Update Guzzle library to version 6.0', '2020-08-19 11:07:29'),
(98, 'October.Drivers', 'comment', '1.1.2', 'Update Guzzle library to version 6.3', '2020-08-19 11:07:29'),
(99, 'RainLab.Builder', 'comment', '1.0.1', 'Initialize plugin.', '2020-08-19 11:07:29'),
(100, 'RainLab.Builder', 'comment', '1.0.2', 'Fixes the problem with selecting a plugin. Minor localization corrections. Configuration files in the list and form behaviors are now autocomplete.', '2020-08-19 11:07:29'),
(101, 'RainLab.Builder', 'comment', '1.0.3', 'Improved handling of the enum data type.', '2020-08-19 11:07:29'),
(102, 'RainLab.Builder', 'comment', '1.0.4', 'Added user permissions to work with the Builder.', '2020-08-19 11:07:29'),
(103, 'RainLab.Builder', 'comment', '1.0.5', 'Fixed permissions registration.', '2020-08-19 11:07:29'),
(104, 'RainLab.Builder', 'comment', '1.0.6', 'Fixed front-end record ordering in the Record List component.', '2020-08-19 11:07:29'),
(105, 'RainLab.Builder', 'comment', '1.0.7', 'Builder settings are now protected with user permissions. The database table column list is scrollable now. Minor code cleanup.', '2020-08-19 11:07:29'),
(106, 'RainLab.Builder', 'comment', '1.0.8', 'Added the Reorder Controller behavior.', '2020-08-19 11:07:29'),
(107, 'RainLab.Builder', 'comment', '1.0.9', 'Minor API and UI updates.', '2020-08-19 11:07:29'),
(108, 'RainLab.Builder', 'comment', '1.0.10', 'Minor styling update.', '2020-08-19 11:07:29'),
(109, 'RainLab.Builder', 'comment', '1.0.11', 'Fixed a bug where clicking placeholder in a repeater would open Inspector. Fixed a problem with saving forms with repeaters in tabs. Minor style fix.', '2020-08-19 11:07:29'),
(110, 'RainLab.Builder', 'comment', '1.0.12', 'Added support for the Trigger property to the Media Finder widget configuration. Names of form fields and list columns definition files can now contain underscores.', '2020-08-19 11:07:29'),
(111, 'RainLab.Builder', 'comment', '1.0.13', 'Minor styling fix on the database editor.', '2020-08-19 11:07:29'),
(112, 'RainLab.Builder', 'comment', '1.0.14', 'Added support for published_at timestamp field', '2020-08-19 11:07:29'),
(113, 'RainLab.Builder', 'comment', '1.0.15', 'Fixed a bug where saving a localization string in Inspector could cause a JavaScript error. Added support for Timestamps and Soft Deleting for new models.', '2020-08-19 11:07:29'),
(114, 'RainLab.Builder', 'comment', '1.0.16', 'Fixed a bug when saving a form with the Repeater widget in a tab could create invalid fields in the form\'s outside area. Added a check that prevents creating localization strings inside other existing strings.', '2020-08-19 11:07:29'),
(115, 'RainLab.Builder', 'comment', '1.0.17', 'Added support Trigger attribute support for RecordFinder and Repeater form widgets.', '2020-08-19 11:07:29'),
(116, 'RainLab.Builder', 'comment', '1.0.18', 'Fixes a bug where \'::class\' notations in a model class definition could prevent the model from appearing in the Builder model list. Added emptyOption property support to the dropdown form control.', '2020-08-19 11:07:29'),
(117, 'RainLab.Builder', 'comment', '1.0.19', 'Added a feature allowing to add all database columns to a list definition. Added max length validation for database table and column names.', '2020-08-19 11:07:29'),
(118, 'RainLab.Builder', 'comment', '1.0.20', 'Fixes a bug where form the builder could trigger the \"current.hasAttribute is not a function\" error.', '2020-08-19 11:07:29'),
(119, 'RainLab.Builder', 'comment', '1.0.21', 'Back-end navigation sort order updated.', '2020-08-19 11:07:29'),
(120, 'RainLab.Builder', 'comment', '1.0.22', 'Added scopeValue property to the RecordList component.', '2020-08-19 11:07:29'),
(121, 'RainLab.Builder', 'comment', '1.0.23', 'Added support for balloon-selector field type, added Brazilian Portuguese translation, fixed some bugs', '2020-08-19 11:07:29'),
(122, 'RainLab.Builder', 'comment', '1.0.24', 'Added support for tag list field type, added read only toggle for fields. Prevent plugins from using reserved PHP keywords for class names and namespaces', '2020-08-19 11:07:29'),
(123, 'RainLab.Builder', 'comment', '1.0.25', 'Allow editing of migration code in the \"Migration\" popup when saving changes in the database editor.', '2020-08-19 11:07:29'),
(124, 'RainLab.Builder', 'comment', '1.0.26', 'Allow special default values for columns and added new \"Add ID column\" button to database editor.', '2020-08-19 11:07:29'),
(125, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2020-08-19 11:07:29'),
(126, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2020-08-19 11:07:29'),
(127, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2020-08-19 11:07:29'),
(128, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2020-08-19 11:07:29'),
(129, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2020-08-19 11:07:29'),
(130, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2020-08-19 11:07:29'),
(131, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2020-08-19 11:07:29'),
(132, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2020-08-19 11:07:29'),
(133, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2020-08-19 11:07:29'),
(134, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2020-08-19 11:07:29'),
(135, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2020-08-19 11:07:29'),
(136, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2020-08-19 11:07:29'),
(137, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2020-08-19 11:07:29'),
(138, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2020-08-19 11:07:29'),
(139, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2020-08-19 11:07:29'),
(140, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2020-08-19 11:07:29'),
(141, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2020-08-19 11:07:29'),
(142, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2020-08-19 11:07:29'),
(143, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2020-08-19 11:07:29'),
(144, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2020-08-19 11:07:29'),
(145, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2020-08-19 11:07:29'),
(146, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2020-08-19 11:07:29'),
(147, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2020-08-19 11:07:29'),
(148, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2020-08-19 11:07:29'),
(149, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2020-08-19 11:07:29'),
(150, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2020-08-19 11:07:29'),
(151, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2020-08-19 11:07:29'),
(152, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2020-08-19 11:07:29'),
(153, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2020-08-19 11:07:30'),
(154, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2020-08-19 11:07:30'),
(155, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2020-08-19 11:07:30'),
(156, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2020-08-19 11:07:30'),
(157, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2020-08-19 11:07:30'),
(158, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2020-08-19 11:07:30'),
(159, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2020-08-19 11:07:30'),
(160, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2020-08-19 11:07:30'),
(161, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2020-08-19 11:07:30'),
(162, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2020-08-19 11:07:30'),
(163, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2020-08-19 11:07:30'),
(164, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2020-08-19 11:07:30'),
(165, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2020-08-19 11:07:30'),
(166, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2020-08-19 11:07:30'),
(167, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2020-08-19 11:07:30'),
(168, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2020-08-19 11:07:30'),
(169, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2020-08-19 11:07:30'),
(170, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2020-08-19 11:07:30'),
(171, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2020-08-19 11:07:30'),
(172, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2020-08-19 11:07:30'),
(173, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2020-08-19 11:07:30'),
(174, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2020-08-19 11:07:30'),
(175, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2020-08-19 11:07:30'),
(176, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2020-08-19 11:07:30'),
(177, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2020-08-19 11:07:30'),
(178, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2020-08-19 11:07:30'),
(179, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2020-08-19 11:07:30'),
(180, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2020-08-19 11:07:30'),
(181, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2020-08-19 11:07:30'),
(182, 'RainLab.Translate', 'comment', '1.6.3', 'Add eager loading for translations, restore support for accessors & mutators', '2020-08-19 11:07:30'),
(183, 'RainLab.Translate', 'comment', '1.6.4', 'Fixes PHP 7.4 compatibility', '2020-08-19 11:07:30'),
(184, 'RainLab.Translate', 'comment', '1.6.5', 'Fixes compatibility issue when other plugins use a custom model morph map', '2020-08-19 11:07:30'),
(185, 'RainLab.Translate', 'script', '1.6.6', 'migrate_morphed_attributes.php', '2020-08-19 11:07:30'),
(186, 'RainLab.Translate', 'comment', '1.6.6', 'Introduce migration to patch existing translations using morph map', '2020-08-19 11:07:30'),
(187, 'RainLab.Translate', 'script', '1.6.7', 'migrate_morphed_indexes.php', '2020-08-19 11:07:30'),
(188, 'RainLab.Translate', 'comment', '1.6.7', 'Introduce migration to patch existing indexes using morph map', '2020-08-19 11:07:30'),
(189, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2020-08-19 11:07:30'),
(190, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2020-08-19 11:07:30'),
(191, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2020-08-19 11:07:30'),
(192, 'RainLab.User', 'comment', '1.0.2', 'Seed tables.', '2020-08-19 11:07:30'),
(193, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2020-08-19 11:07:30'),
(194, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2020-08-19 11:07:30'),
(195, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2020-08-19 11:07:30'),
(196, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2020-08-19 11:07:30'),
(197, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2020-08-19 11:07:30'),
(198, 'RainLab.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2020-08-19 11:07:30'),
(199, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2020-08-19 11:07:30'),
(200, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2020-08-19 11:07:30'),
(201, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2020-08-19 11:07:30'),
(202, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2020-08-19 11:07:30'),
(203, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2020-08-19 11:07:30'),
(204, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2020-08-19 11:07:30'),
(205, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2020-08-19 11:07:30'),
(206, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2020-08-19 11:07:30'),
(207, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2020-08-19 11:07:30'),
(208, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2020-08-19 11:07:30'),
(209, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2020-08-19 11:07:30'),
(210, 'RainLab.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2020-08-19 11:07:30'),
(211, 'RainLab.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2020-08-19 11:07:30'),
(212, 'RainLab.User', 'script', '1.1.1', 'create_user_groups_table.php', '2020-08-19 11:07:30'),
(213, 'RainLab.User', 'script', '1.1.1', 'seed_user_groups_table.php', '2020-08-19 11:07:30'),
(214, 'RainLab.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2020-08-19 11:07:30'),
(215, 'RainLab.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2020-08-19 11:07:30'),
(216, 'RainLab.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2020-08-19 11:07:30'),
(217, 'RainLab.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2020-08-19 11:07:30'),
(218, 'RainLab.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2020-08-19 11:07:30'),
(219, 'RainLab.User', 'script', '1.2.0', 'users_add_deleted_at.php', '2020-08-19 11:07:30'),
(220, 'RainLab.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2020-08-19 11:07:30'),
(221, 'RainLab.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2020-08-19 11:07:30'),
(222, 'RainLab.User', 'comment', '1.2.2', 'Add bulk action button to user list.', '2020-08-19 11:07:30'),
(223, 'RainLab.User', 'comment', '1.2.3', 'Included some descriptive paragraphs in the Reset Password component markup.', '2020-08-19 11:07:30'),
(224, 'RainLab.User', 'comment', '1.2.4', 'Added a checkbox for blocking all mail sent to the user.', '2020-08-19 11:07:30'),
(225, 'RainLab.User', 'script', '1.2.5', 'update_timestamp_nullable.php', '2020-08-19 11:07:30'),
(226, 'RainLab.User', 'comment', '1.2.5', 'Database maintenance. Updated all timestamp columns to be nullable.', '2020-08-19 11:07:30'),
(227, 'RainLab.User', 'script', '1.2.6', 'users_add_last_seen.php', '2020-08-19 11:07:30'),
(228, 'RainLab.User', 'comment', '1.2.6', 'Add a dedicated last seen column for users.', '2020-08-19 11:07:30'),
(229, 'RainLab.User', 'comment', '1.2.7', 'Minor fix to user timestamp attributes.', '2020-08-19 11:07:30'),
(230, 'RainLab.User', 'comment', '1.2.8', 'Add date range filter to users list. Introduced a logout event.', '2020-08-19 11:07:30'),
(231, 'RainLab.User', 'comment', '1.2.9', 'Add invitation mail for new accounts created in the back-end.', '2020-08-19 11:07:30'),
(232, 'RainLab.User', 'script', '1.3.0', 'users_add_guest_flag.php', '2020-08-19 11:07:30'),
(233, 'RainLab.User', 'script', '1.3.0', 'users_add_superuser_flag.php', '2020-08-19 11:07:30'),
(234, 'RainLab.User', 'comment', '1.3.0', 'Introduced guest user accounts.', '2020-08-19 11:07:30'),
(235, 'RainLab.User', 'comment', '1.3.1', 'User notification variables can now be extended.', '2020-08-19 11:07:30'),
(236, 'RainLab.User', 'comment', '1.3.2', 'Minor fix to the Auth::register method.', '2020-08-19 11:07:30'),
(237, 'RainLab.User', 'comment', '1.3.3', 'Allow prevention of concurrent user sessions via the user settings.', '2020-08-19 11:07:30'),
(238, 'RainLab.User', 'comment', '1.3.4', 'Added force secure protocol property to the account component.', '2020-08-19 11:07:30'),
(239, 'RainLab.User', 'comment', '1.4.0', '!!! The Notifications tab in User settings has been removed.', '2020-08-19 11:07:30'),
(240, 'RainLab.User', 'comment', '1.4.1', 'Added support for user impersonation.', '2020-08-19 11:07:30'),
(241, 'RainLab.User', 'comment', '1.4.2', 'Fixes security bug in Password Reset component.', '2020-08-19 11:07:30'),
(242, 'RainLab.User', 'comment', '1.4.3', 'Fixes session handling for AJAX requests.', '2020-08-19 11:07:30'),
(243, 'RainLab.User', 'comment', '1.4.4', 'Fixes bug where impersonation touches the last seen timestamp.', '2020-08-19 11:07:30'),
(244, 'RainLab.User', 'comment', '1.4.5', 'Added token fallback process to Account / Reset Password components when parameter is missing.', '2020-08-19 11:07:30'),
(245, 'RainLab.User', 'comment', '1.4.6', 'Fixes Auth::register method signature mismatch with core OctoberCMS Auth library', '2020-08-19 11:07:30'),
(246, 'RainLab.User', 'comment', '1.4.7', 'Fixes redirect bug in Account component / Update translations and separate user and group management.', '2020-08-19 11:07:30'),
(247, 'RainLab.User', 'comment', '1.4.8', 'Fixes a bug where calling MailBlocker::removeBlock could remove all mail blocks for the user.', '2020-08-19 11:07:30'),
(248, 'RainLab.User', 'comment', '1.5.0', '!!! Required password length is now a minimum of 8 characters. Previous passwords will not be affected until the next password change.', '2020-08-19 11:07:30'),
(249, 'RainLab.User', 'script', '1.5.1', 'users_add_ip_address.php', '2020-08-19 11:07:30'),
(250, 'RainLab.User', 'comment', '1.5.1', 'User IP addresses are now logged. Introduce registration throttle.', '2020-08-19 11:07:30'),
(251, 'RainLab.User', 'comment', '1.5.2', 'Whitespace from usernames is now trimmed, allowed for username to be added to Reset Password mail templates.', '2020-08-19 11:07:30'),
(252, 'RainLab.User', 'comment', '1.5.3', 'Fixes a bug in the user update functionality if password is not changed. Added highlighting for banned users in user list.', '2020-08-19 11:07:30'),
(253, 'Bboxdigi.Bale', 'script', '1.0.16', 'add_description_to_neighbors_table.php', '2020-08-20 10:39:39'),
(254, 'Bboxdigi.Bale', 'comment', '1.0.16', 'Add description to neighbors table', '2020-08-20 10:39:39'),
(255, 'Bboxdigi.Bale', 'script', '1..0..17', 'change_number_in_flats_table.php', '2020-08-21 09:00:21'),
(256, 'Bboxdigi.Bale', 'comment', '1..0..17', 'Change number in flats table', '2020-08-21 09:00:21'),
(257, 'Bboxdigi.Bale', 'script', '1.0.17', 'change_number_in_flats_table.php', '2020-08-24 06:03:50'),
(258, 'Bboxdigi.Bale', 'comment', '1.0.17', 'Change number in flats table', '2020-08-24 06:03:50'),
(259, 'Bboxdigi.Bale', 'script', '1.0.18', 'add_plan_svg_to_flats_table.php', '2020-08-24 06:03:50'),
(260, 'Bboxdigi.Bale', 'comment', '1.0.18', 'Add plan svg to flats table', '2020-08-24 06:03:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'Bboxdigi.Bale', '1.0.18', '2020-08-24 06:03:50', 0, 0),
(2, 'Indikator.Backend', '1.6.12', '2020-08-19 11:07:29', 0, 0),
(3, 'October.Drivers', '1.1.2', '2020-08-19 11:07:29', 0, 0),
(4, 'RainLab.Builder', '1.0.26', '2020-08-19 11:07:29', 0, 0),
(5, 'RainLab.Translate', '1.6.7', '2020-08-19 11:07:30', 0, 0),
(6, 'RainLab.User', '1.5.3', '2020-08-19 11:07:30', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `created_ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Guest', 'guest', 'Default group for guest users.', '2020-08-19 11:07:30', '2020-08-19 11:07:30'),
(2, 'Registered', 'registered', 'Default group for registered users.', '2020-08-19 11:07:30', '2020-08-19 11:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_throttle`
--

CREATE TABLE `user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `bboxdigi_bale_abouts`
--
ALTER TABLE `bboxdigi_bale_abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_contacts`
--
ALTER TABLE `bboxdigi_bale_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_flats`
--
ALTER TABLE `bboxdigi_bale_flats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_floors`
--
ALTER TABLE `bboxdigi_bale_floors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_footers`
--
ALTER TABLE `bboxdigi_bale_footers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_headers`
--
ALTER TABLE `bboxdigi_bale_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_index_pages`
--
ALTER TABLE `bboxdigi_bale_index_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_infrastructures`
--
ALTER TABLE `bboxdigi_bale_infrastructures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_infrastructure_points`
--
ALTER TABLE `bboxdigi_bale_infrastructure_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_infrastructure_solutions`
--
ALTER TABLE `bboxdigi_bale_infrastructure_solutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_interriors`
--
ALTER TABLE `bboxdigi_bale_interriors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_locations`
--
ALTER TABLE `bboxdigi_bale_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_location_categories`
--
ALTER TABLE `bboxdigi_bale_location_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bboxdigi_bale_neighbors`
--
ALTER TABLE `bboxdigi_bale_neighbors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indikator_backend_trash`
--
ALTER TABLE `indikator_backend_trash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_user_mail_blockers_email_index` (`email`),
  ADD KEY `rainlab_user_mail_blockers_template_index` (`template`),
  ADD KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_login_unique` (`username`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`),
  ADD KEY `users_login_index` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_groups_code_index` (`code`);

--
-- Indexes for table `user_throttle`
--
ALTER TABLE `user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_throttle_user_id_index` (`user_id`),
  ADD KEY `user_throttle_ip_address_index` (`ip_address`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_abouts`
--
ALTER TABLE `bboxdigi_bale_abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_contacts`
--
ALTER TABLE `bboxdigi_bale_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_flats`
--
ALTER TABLE `bboxdigi_bale_flats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_floors`
--
ALTER TABLE `bboxdigi_bale_floors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_footers`
--
ALTER TABLE `bboxdigi_bale_footers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_headers`
--
ALTER TABLE `bboxdigi_bale_headers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_index_pages`
--
ALTER TABLE `bboxdigi_bale_index_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_infrastructures`
--
ALTER TABLE `bboxdigi_bale_infrastructures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_infrastructure_points`
--
ALTER TABLE `bboxdigi_bale_infrastructure_points`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_infrastructure_solutions`
--
ALTER TABLE `bboxdigi_bale_infrastructure_solutions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_interriors`
--
ALTER TABLE `bboxdigi_bale_interriors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_locations`
--
ALTER TABLE `bboxdigi_bale_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_location_categories`
--
ALTER TABLE `bboxdigi_bale_location_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bboxdigi_bale_neighbors`
--
ALTER TABLE `bboxdigi_bale_neighbors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `indikator_backend_trash`
--
ALTER TABLE `indikator_backend_trash`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;
--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_throttle`
--
ALTER TABLE `user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
