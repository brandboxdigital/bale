<?php namespace Bboxdigi\Bale\Components;

use Cms\Classes\ComponentBase;
use Mail;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact Component',
            'description' => 'Component for the contact form'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSend()
    {
      $data = [
        'name' => post('name'),
        'phone' => post('phone'),
        'text' => post('message')
      ];

      Mail::send('bboxdigi.bale::mail.contact', $data, function($message) {
        $message->to(\Bboxdigi\Bale\Models\Contact::first()->contact_form_email);
        $message->subject('Contact request received');
      });
    }
}
