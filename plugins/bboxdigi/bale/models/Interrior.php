<?php namespace Bboxdigi\Bale\Models;

use Model;

/**
 * Interrior Model
 */
class Interrior extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bboxdigi_bale_interriors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'heading' => 'required',
        'subheading' => 'required'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'picture1' => 'System\Models\File',
        'picture2' => 'System\Models\File',
    ];
    public $attachMany = [
        'pictures' => 'System\Models\File',
        'pictures_2' => 'System\Models\File'
    ];

    public function getHeadingHtml()
    {
        return str_replace("\n", '<br>', $this->heading);
    }

    public function getText1Html()
    {
        return str_replace("\n", '<br>', $this->text1);
    }

    public function getAddress1Html()
    {
        return str_replace("\n", '<br>', $this->address1);
    }

    public function getText2Html()
    {
        return str_replace("\n", '<br>', $this->text2);
    }

    public function getAddress2Html()
    {
        return str_replace("\n", '<br>', $this->address2);
    }

    public function getSlider2ThumbUrl($picture)
    {
        return $picture->getThumb(800, 800, [ 'mode' => 'crop' ]);
    }

    public function getSlider2DescriptionHtml($text)
    {
        return str_replace("\n", '<br>', $text);
    }
}
