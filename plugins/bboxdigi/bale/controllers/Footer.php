<?php namespace Bboxdigi\Bale\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Footer Back-end Controller
 */
class Footer extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController'
    ];

    public $formConfig = 'config_form.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Bale', 'bale', 'footer');
    }
}
