<?php namespace Bboxdigi\Bale\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Contact Back-end Controller
 */
class Contact extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController'
    ];

    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Bale', 'bale', 'contact');
    }
}
