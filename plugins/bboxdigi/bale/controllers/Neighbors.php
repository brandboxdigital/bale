<?php namespace Bboxdigi\Bale\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Neighbors Back-end Controller
 */
class Neighbors extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bboxdigi.Bale', 'bale', 'neighbors');
    }
}
