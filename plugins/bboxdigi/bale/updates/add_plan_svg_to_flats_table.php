<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Infrastructure;

class AddPlanSvgToFlatsTable extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_bale_flats', function (Blueprint $table) {
          $table->text('plan_svg')->nullable();
        });

        (new Infrastructure())->forceSave();
    }

    public function down()
    {
      Schema::table('bboxdigi_bale_flats', function(Blueprint $table) {
        $table->dropColumn('plan_svg');
      });
    }
}
