<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\About;

class CreateAboutsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_abouts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->text('heading');
            $table->text('subheading');
            $table->text('properties');
            $table->text('overview_link');
            $table->text('overview_heading');
            $table->text('overview_text');
            $table->text('history_heading');
            $table->text('history_text');
            $table->text('first_owner_heading');
            $table->text('first_owner_text');
            $table->text('neighbors_heading');
            $table->text('neighbors_text');
            $table->increments('id');
            $table->timestamps();
        });

        $about = new About([
            'heading' => "ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней",
            'subheading' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.',
            'properties' => 'Особенности дома',
            'overview_link' => 'Обзор Резиденций',
            'overview_heading' => 'Дом расположен между прекрасных дворцов и храмов, великолепных бульваров, парков и каналов',
            'overview_text' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.',
            'history_heading' => 'История Дома Балле',
            'history_text' => 'В 1786 году земельный участок от Никольской улицы до Екатерининского канала был пожалован генерал-адьютанту Ивану Балле. Владелец разделил участок на четыре части, оставив за собой угловую. На плане Петербурга 1798 года на этом месте уже обозначен каменный дом, обращенный главным фасадом на площадь. Имя его первого архитектора нам не известно. В 1828 году архитектор Давид Висконти соединил здание с флигелем, а во дворе был разбит сад с фонтаном и нимфой. строители и специалисты по парковой.',
            'first_owner_heading' => 'Первый владелец дома',
            'first_owner_text' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.',
            'neighbors_heading' => 'Великие и знаменитые соседи',
            'neighbors_text' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.'
        ]);

        $about->save();

        $picture1 = (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju5.jpg');
        $picture1->title = 'Бассейн';
        $picture1->description = 'Длинной 17 м, сауна, хамам и массажная';

        $picture2 = (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju6.jpg');
        $picture2->title = 'Some other title';
        $picture2->description = 'consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem';

        $picture3 = (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju5.jpg');
        $picture3->title = 'Бассейн';
        $picture3->description = 'Длинной 17 м, сауна, хамам и массажная';

        $picture4 = (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju6.jpg');
        $picture4->title = 'Some other title';
        $picture4->description = 'consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem';

        $about->pictures1()->saveMany([
            $picture1,
            $picture2,
            $picture3,
            $picture4
        ]);

        $about->pictures2()->saveMany([
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju1.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju2.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju1.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju2.jpg')
        ]);

        $about->pictures3()->saveMany([
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju3.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju4.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju3.jpg'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/parmaju4.jpg')
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_abouts');
    }
}
