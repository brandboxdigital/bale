<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\InfrastructureSolution;

class CreateInfrastructureSolutionsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_infrastructure_solutions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('description');
            $table->timestamps();
        });

        $this->seed();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_infrastructure_solutions');
    }

    protected function seed()
    {
        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'водопровод: два независимых ввода и система очистки' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf01.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'два независимых источника электроснабжения, встроенный ТП 10/04' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf02.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'единая сеть WiFi во всем здании высокоскоростной интернет, ТВ' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf03.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'IP-телефония от одного из двух ведущих провайдеров на выбор собственника' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf04.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'каждые апартаменты оборудованы собственной вентиляционной системой машиной' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf05.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'единая система контроля кондиционирования, отопления, влажности дает возможность установки индивидуальных параметров на каждое помещение' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf06.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'система усиления зоны мобильной связи во всех помещениях, включая лифтовые кабины и паркинг' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf07.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'охранная система каждой квартиры интегрирована в единый комплекс автоматизации апартаментов' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf08.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'персонализированный контроль доступа в помещения здания' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf09.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'видеонаблюдение в общих зонах' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf10.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'теплоснабжение обеспечивают два источника – городская теплосеть и автономный источник на природном газе' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf11.svg');
        $infrastructure_solution->save();

        $infrastructure_solution = new InfrastructureSolution([ 'description' => 'разграничение прав доступа к индивидуальным лифтам' ]);
        $infrastructure_solution->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/inf12.svg');
        $infrastructure_solution->save();
    }
}
