<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Infrastructure;

class CreateInfrastructuresTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_infrastructures', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('heading')->default("ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней");
            $table->string('subheading', 4096)->default("Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.");
            $table->string('subheading2')->default('Общие технические системы и решения');
            $table->string('overview_link')->default('Обзор Резиденций');
            $table->timestamps();
        });

        (new Infrastructure())->forceSave();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_infrastructures');
    }
}
