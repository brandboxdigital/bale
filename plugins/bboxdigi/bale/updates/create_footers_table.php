<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Footer;

class CreateFootersTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_footers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email')->default('info@домбалле.ru');
            $table->string('phone')->default('8 499 945-294-02');
            $table->string('copyright')->default('Copyright 2019 Дом Балле');
            $table->string('design')->default('Design by brandbox.lv');
            $table->string('address', 4096)->default('Дом Балле Санкт-Петербург, Адмиралтейский район, 190068 ул. Глинки, дом 4, литер «А» (Театральная площадь, 14.)');
            $table->timestamps();
        });

        $footer = new Footer();
        $footer->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/footer_img.svg');
        $footer->forceSave();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_footers');
    }
}
