<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Interrior;

class CreateInterriorsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_interriors', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('heading')->default("ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней");
            $table->string('subheading', 4096)->default('Сохраняя историческую основу и сложившийся облик здания, создана среда, которая отвечает последнему слову техники, обеспечивая наилучшие возможности для комфортной, защищенной и радостной жизни.');
            $table->string('caption1')->default('KRASSKY');
            $table->string('text1', 4096)->default("Основанная в Латвии международная компания – KRASSKY – принимала участие в работе над отделкой̆ Дома Балле.\nКлиентами KRASSKY являются люди с высокими требованиями к сервису и богатым жизненным опытом. Мы считаем, что хороший̆ интерьер подчеркивает личность и стиль жизни его пользователя, поэтому для каждого клиента создаются индивидуальные решения. Мы верим, что хороший̆ дизайн улучшает качество жизни, долговечен и функционален.");
            $table->string('address1', 1024)->default("119021, Москва, ул. Тимура Фрунзе 11/1\n+7 (495) 181-53-00\nkrassky@krassky.ru");
            $table->string('caption2')->default('KRASSKY');
            $table->string('text2', 4096)->default("Основанная в Латвии международная компания – KRASSKY – принимала участие в работе над отделкой̆ Дома Балле.\nКлиентами KRASSKY являются люди с высокими требованиями к сервису и богатым жизненным опытом. Мы считаем, что хороший̆ интерьер подчеркивает личность и стиль жизни его пользователя, поэтому для каждого клиента создаются индивидуальные решения. Мы верим, что хороший̆ дизайн улучшает качество жизни, долговечен и функционален.");
            $table->string('address2', 1024)->default("119021, Москва, ул. Тимура Фрунзе 11/1\n+7 (495) 181-53-00\nkrassky@krassky.ru");
            $table->string('overview_link')->default('Обзор Резиденций');
            $table->timestamps();
        });

        $interrior = new Interrior();
        $interrior->picture1 = (new \System\Models\File)->fromFile('themes/bale/assets/images/interrior_placeholder.jpg');
        $interrior->picture2 = (new \System\Models\File)->fromFile('themes/bale/assets/images/interrior_placeholder.jpg');
        $interrior->forceSave();
        
        $interrior->pictures()->saveMany([
            (new \System\Models\File)->fromFile('themes/bale/assets/images/details1.png'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/details1.png'),
            (new \System\Models\File)->fromFile('themes/bale/assets/images/details1.png')
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_interriors');
    }
}
