<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Floor;

class CreateFloorsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_floors', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('number');
            $table->text('description');
            $table->timestamps();
        });

        $this->seed();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_floors');
    }

    protected function seed()
    {
        Floor::create([
            'number' => 1,
            'description' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor'
        ]);

        Floor::create([
            'number' => 2,
            'description' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor'
        ]);

        Floor::create([
            'number' => 3,
            'description' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor'
        ]);

        Floor::create([
            'number' => 4,
            'description' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor'
        ]);
    }
}
