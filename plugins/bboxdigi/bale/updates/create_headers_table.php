<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Header;

class CreateHeadersTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_headers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('about')->default('О ДОМЕ');
            $table->string('location')->default('МЕСТОПОЛОЖЕНИЕ');
            $table->string('infrastructure')->default('ИНФРАСТРУКТУРА');
            $table->string('interrior')->default('ИНТЕРЬЕРЫ');
            $table->string('overview')->default('ОБЗОР РЕЗИДЕНЦИЙ');
            $table->string('contact')->default('КОНТАКТЫ');
            $table->string('phone')->default('+7 812 770 22 57');
            $table->timestamps();
        });

        $header = new Header();
        $header->logo = (new \System\Models\File)->fromFile('themes/bale/assets/images/logo.png');
        $header->forceSave();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_headers');
    }
}
