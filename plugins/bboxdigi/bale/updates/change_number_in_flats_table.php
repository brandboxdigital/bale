<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Infrastructure;

class ChangeNumberInFlatsTable extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_bale_flats', function (Blueprint $table) {
          $table->string('number')->change();
        });
    }

    public function down()
    {
      Schema::table('bboxdigi_bale_flats', function(Blueprint $table) {
        $table->dropColumn('number');
        $table->integer('number')->nullable();
      });
    }
}
