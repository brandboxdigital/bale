<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Infrastructure;

class AddDescriptionToNeighborsTable extends Migration
{
    public function up()
    {
        Schema::table('bboxdigi_bale_neighbors', function (Blueprint $table) {
          $table->text('description')->nullable();
        });

        (new Infrastructure())->forceSave();
    }

    public function down()
    {
      Schema::table('bboxdigi_bale_neighbors', function(Blueprint $table) {
        $table->dropColumn('description');
      });
    }
}
