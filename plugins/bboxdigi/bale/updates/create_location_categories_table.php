<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\LocationCategory;

class CreateLocationCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_location_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        LocationCategory::create(['name' => 'Сакральные объекты']);
        LocationCategory::create(['name' => 'Музеи']);
        LocationCategory::create(['name' => 'Театры']);
        LocationCategory::create(['name' => 'Рестораны']);
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_location_categories');
    }
}
