<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Neighbor;

class CreateNeighborsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_neighbors', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $this->seed();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_neighbors');
    }

    protected function seed()
    {
        Neighbor::create([
            'name' => 'Михаил Лермонтов',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person1.png')
        ]);

        Neighbor::create([
            'name' => 'Николай Гоголь',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person2.png')
        ]);

        Neighbor::create([
            'name' => 'Михаил Лермонтов',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person1.png')
        ]);

        Neighbor::create([
            'name' => 'Николай Гоголь',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person2.png')
        ]);

        Neighbor::create([
            'name' => 'Михаил Лермонтов',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person1.png')
        ]);

        Neighbor::create([
            'name' => 'Николай Гоголь',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person2.png')
        ]);

        Neighbor::create([
            'name' => 'Михаил Лермонтов',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person1.png')
        ]);

        Neighbor::create([
            'name' => 'Николай Гоголь',
            'picture' => (new \System\Models\File)->fromFile('themes/bale/assets/images/person2.png')
        ]);

    }
}
