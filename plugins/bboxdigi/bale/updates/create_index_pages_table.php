<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\IndexPage;

class CreateIndexPagesTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_index_pages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('heading')->default("ДОМ БАЛЛЕ -\nимперская роскошь\nнаших дней");
            $table->timestamps();
        });

        (new IndexPage())->forceSave();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_index_pages');
    }
}
