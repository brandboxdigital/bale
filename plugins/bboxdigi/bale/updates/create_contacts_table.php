<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\Contact;

class CreateContactsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_contacts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('contact_form_email')->default('');
            $table->string('email')->default('info@домбалле.ru');
            $table->string('phone')->default('8 499 945-294-02');
            $table->string('address')->default("Дом Балле, Санкт-Петербург,\nАдмиралтейский район, 190068\nул. Глинки, дом 4, литер «А»\n(Театральная площадь, 14.)");
            $table->string('heading')->default('Запрос на обратный звонок');
            $table->string('tos')->default('Я согласен на обработку моих персональных данных');
            $table->string('submit')->default('0тпрabить зaявку');
            $table->timestamps();
        });

        (new Contact())->forceSave();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_contacts');
    }
}
