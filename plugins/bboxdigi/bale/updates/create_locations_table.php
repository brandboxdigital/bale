<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\LocationCategory;
use Bboxdigi\Bale\Models\Location;

class CreateLocationsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_locations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('location_category_id');
            $table->string('name');
            $table->text('description');
            $table->string('distance');
            $table->float('position_x');
            $table->float('position_y');
            $table->timestamps();
        });

        $this->seed();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_locations');
    }

    protected function seed()
    {
        $location_category = LocationCategory::where('name', 'Сакральные объекты')->first();
        if($location_category) {
            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.40,
                'position_x' => 0.33
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.5,
                'position_x' => 0.23
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Римско-католический храм Св. Станислава',
                'distance' => '650 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.5,
                'position_x' => 0.23
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();
        }

        $location_category = LocationCategory::where('name', 'Музеи')->first();
        if($location_category) {
            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.30,
                'position_x' => 0.43
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.26,
                'position_x' => 0.45
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Римско-католический храм Св. Станислава',
                'distance' => '650 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.8,
                'position_x' => 0.53
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();
        }

        $location_category = LocationCategory::where('name', 'Театры')->first();
        if($location_category) {
            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.30,
                'position_x' => 0.53
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.6,
                'position_x' => 0.43
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Римско-католический храм Св. Станислава',
                'distance' => '650 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.76,
                'position_x' => 0.25
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();
        }

        $location_category = LocationCategory::where('name', 'Рестораны')->first();
        if($location_category) {
            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.26,
                'position_x' => 0.45
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Никольский сад',
                'distance' => '220 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.9,
                'position_x' => 0.23
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();

            $location = new Location([
                'location_category_id' => $location_category->id,
                'name' => 'Римско-католический храм Св. Станислава',
                'distance' => '650 м',
                'description' => 'Храм св. Станислава был построен на месте бывшего дома первого католического митрополита Российской империи Станислава Богуш-Сестренцевича, который, переехав в другой дом, пожертвовал на строительство храма участок земли на углу улиц Мастерской и Торговой (ныне Союза Печатников) и деньги.строители и специалисты по парковой.',
                'position_y' => 0.5,
                'position_x' => 0.53
            ]);
            $location->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/location_placeholder.jpg');
            $location->save();
        }
    }
}
