<?php namespace Bboxdigi\Bale\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

use Bboxdigi\Bale\Models\InfrastructurePoint;

class CreateInfrastructurePointsTable extends Migration
{
    public function up()
    {
        Schema::create('bboxdigi_bale_infrastructure_points', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        $this->seed();
    }

    public function down()
    {
        Schema::dropIfExists('bboxdigi_bale_infrastructure_points');
    }

    protected function seed()
    {
        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Бассейн',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Консерж Сервис',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Зал для парикмахера',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Переговоровнаяс',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Спортзал',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Специальное помещение для хранения вин',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Kамера для хранения одежды из меха',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();

        $infrastructure_point = new InfrastructurePoint([
            'name' => 'Подземный паркинг на 42 автомобиля',
            'description' => 'Дом Балле в Санкт-Петербурге – один из тех памятников градостроения Северной столицы, которые переживают новое рождение, органично входят в XXI век, сохраняя при том дух блистательного прошлого.'
        ]);
        $infrastructure_point->picture = (new \System\Models\File)->fromFile('themes/bale/assets/images/infrastructure.png');
        $infrastructure_point->save();
    }
}
