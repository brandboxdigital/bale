<?php namespace Bboxdigi\Bale;

use Backend;
use System\Classes\PluginBase;

/**
 * Bale Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Info',
            'description' => 'No description provided yet...',
            'author'      => 'bboxdigi',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Bboxdigi\Bale\Components\Contact' => 'contactComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'bboxdigi.polls.some_permission' => [
                'tab' => 'polls',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
          'bale' => [
              'label'       => 'Info',
              'url'         => Backend::url('bboxdigi/bale/indexpage/update/1'),
              'icon'        => 'icon-leaf',
              'order'       => 1,
              'counter' => false,
              'sideMenu' => [
                  'header' => [
                      'label' => 'Header',
                      'url' => Backend::url('bboxdigi/bale/header/update/1'),
                      'icon' => 'icon-chevron-up'
                  ],
                  'footer' => [
                      'label' => 'Footer',
                      'url' => Backend::url('bboxdigi/bale/footer/update/1'),
                      'icon' => 'icon-chevron-down'
                  ],
                  'indexpage' => [
                      'label' => 'Index Page',
                      'url' => Backend::url('bboxdigi/bale/indexpage/update/1'),
                      'icon' => 'icon-file'
                  ],
                  'about' => [
                      'label' => 'About Page',
                      'url' => Backend::url('bboxdigi/bale/about/update/1'),
                      'icon' => 'icon-question'
                  ],
                  'neighbors' => [
                      'label' => 'Neighbors',
                      'url' => Backend::url('bboxdigi/bale/neighbors'),
                      'icon' => 'icon-user-circle'
                  ],
                  'locationcategories' => [
                      'label' => 'Locations',
                      'url' => Backend::url('bboxdigi/bale/locationcategories'),
                      'icon' => 'icon-map-marker'
                  ],
                  'infrastructure' => [
                      'label' => 'Infrastructure',
                      'url' => Backend::url('bboxdigi/bale/infrastructure/update/1'),
                      'icon' => 'icon-wrench'
                  ],
                  'infrastructurepoints' => [
                      'label' => 'Infrastructure Points',
                      'url' => Backend::url('bboxdigi/bale/infrastructurepoints'),
                      'icon' => 'icon-dot-circle-o'
                  ],
                  'infrastructuresolutions' => [
                      'label' => 'Infrastructure Solutions',
                      'url' => Backend::url('bboxdigi/bale/infrastructuresolutions'),
                      'icon' => 'icon-list'
                  ],
                  'interrior' => [
                      'label' => 'Interrior',
                      'url' => Backend::url('bboxdigi/bale/interrior/update/1'),
                      'icon' => 'icon-building'
                  ],
                  'floors' => [
                      'label' => 'Floors',
                      'url' => Backend::url('bboxdigi/bale/floors'),
                      'icon' => 'icon-bars'
                  ],
                  'contact' => [
                      'label' => 'Contact',
                      'url' => Backend::url('bboxdigi/bale/contact/update/1'),
                      'icon' => 'icon-phone'
                  ]
              ]
          ],
      ];
    }
}
